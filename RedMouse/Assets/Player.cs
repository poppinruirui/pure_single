﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Photon.PunBehaviour , IPunObservable 
{
	GameObject m_goBalls = null;

	public bool _isRebornProtected = true;

	public Color _color_inner;
	public Color _color_ring;
	public Color _color_poison;

	// Use this for initialization
	void Start () 
	{
		
	}

	void Awake()
	{
		Init ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Init()
	{
		_isRebornProtected = true; // 第一次出生时一定是处于“重生保护状态”

		if (photonView.isMine) {
			this.gameObject.name = "MainPlayer";     
		} else {
			this.gameObject.name = "player_" + photonView.ownerId;
		}
		m_goBalls = GameObject.Find ( "Balls" );
		this.gameObject.transform.parent = m_goBalls.transform;

		int color_index = 0;
		_color_ring = ColorPalette.RandomOuterRingColor (ref color_index);
		_color_inner = ColorPalette.RandomInnerFillColor ( color_index );
		_color_poison = ColorPalette.RandomPoisonFillColor ( color_index);
	}

	public void PhotonRemoveObject( GameObject go )
	{
		PhotonNetwork.Destroy( go );
	}

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{

	}
	public void SetRebornProtected( bool val )
	{
		photonView.RPC ( "RPC_SetRebornProtected", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetRebornProtected(  bool val )
	{
		_isRebornProtected = val;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
public class UI_MouseMode : MonoBehaviour
{
    Button m_btnMouseMode;
    Main m_Main;

    // Use this for initialization
    void Start ()
    {
        GameObject go;
        go = GameObject.Find("UI/btnMouseMode");//获取按钮游戏对象
        m_btnMouseMode  = (Button)go.GetComponent<Button>();//获取按钮脚本组件
        m_btnMouseMode.onClick.AddListener(onClickBtnMouseMode); //添加点击侦听

        go = GameObject.Find("Main Camera");
        m_Main = (Main)go.GetComponent<Main>();
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void onClickBtnMouseMode()
    {
        m_Main.ToggleMouseMode();
    }


}

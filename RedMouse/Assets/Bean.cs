﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bean : Ball {

	// Use this for initialization
	void Start ()
	{
		
	}

	void Awake()
	{
		_balltype = eBallType.ball_type_bean;

		Init ();
	}

	public override void  Init ()
	{
		Transform transOuuter = this.gameObject.transform.FindChild ("InnerFill"); 
		if (transOuuter) {
			m_goOutterBall = transOuuter.gameObject;
		}

		if ( m_goOutterBall == null)
		{
			isInited = false;
			Debug.LogError ( "木得外环怎么行" );
			return;
		}


		_collider = m_goOutterBall.GetComponent<CircleCollider2D> ();

		Transform transMainTrigger = this.gameObject.transform.FindChild ("MainTrigger"); 
		if ( transMainTrigger )
		{

			_trigger = transMainTrigger.gameObject;
			_colliderTrigger = _trigger.GetComponent<CircleCollider2D> ();
			_triggersr = _trigger.GetComponent<SpriteRenderer> ();
		}


	}
	
	// Update is called once per frame
	public override void Update () {
	//	BeanSpray ();

		RefeshPosDueToOutOfScreen ();
	}

	public float GetBeanSize()
	{
		return Main.BALL_MIN_SIZE;
	}

	bool m_bIsSpraying = false;

	public void BeginBeanSpray( float fKX, float fKY )
	{
		m_bIsSpraying = true;
		_kx = fKX;
		_ky = fKY;
	}

	public void BeanSpray()
	{
		if (!m_bIsSpraying) {
			return;
		}

		Vector3 vecTemp = this.gameObject.transform.position;
		vecTemp.x += 1.0f * _kx;
		vecTemp.y += 1.0f * _ky;
		this.gameObject.transform.position = vecTemp;
	}

	public void EndBeanSpray()
	{
		m_bIsSpraying = false;
	}

}

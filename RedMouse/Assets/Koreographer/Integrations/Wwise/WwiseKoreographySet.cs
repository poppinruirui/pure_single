﻿//----------------------------------------------
//            	   Koreographer                 
//    Copyright © 2014-2017 Sonic Bloom, LLC    
//----------------------------------------------

using UnityEngine;
using System.Collections.Generic;

namespace SonicBloom.Koreo.Players.Wwise
{
	public class WwiseKoreographySet : ScriptableObject
	{
		[Tooltip("The Koreography contained in this set.")]
		public List<WwiseKoreoMediaIDEntry> koreographies;
	}

	[System.Serializable]
	public class WwiseKoreoMediaIDEntry
	{
		public Koreography koreo = null;
		
		[SerializeField]
		uint mediaID = 0;

		/// <summary>
		/// Gets the MediaID. WARNING: The setter only exists in the Unity Editor! You should not
		/// modify the MediaID directly unless you are ABSOLUTELY sure you know what you're doing.
		/// </summary>
		/// <value>The Wwise MediaID for the audio file used by the specified Koreography.</value>
		public uint MediaID
		{
			get
			{
				return mediaID;
			}
#if UNITY_EDITOR
			set
			{
				mediaID = value;
			}
#endif
		}
	}
}

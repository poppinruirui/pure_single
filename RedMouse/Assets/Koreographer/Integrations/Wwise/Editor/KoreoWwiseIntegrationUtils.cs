﻿//----------------------------------------------
//            	   Koreographer                 
//    Copyright © 2014-2017 Sonic Bloom, LLC    
//----------------------------------------------

using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml.XPath;
using System.Collections.Generic;
using SonicBloom.Koreo.Players.Wwise;

namespace SonicBloom.Koreo.EditorUI.WwiseTools
{
	public static class WwiseIntegrationUtils
	{
		/// <summary>
		/// Updates the Media IDs for all Koreography in the entire project.
		/// </summary>
		[MenuItem("Assets/Wwise/Koreographer/Update Koreography Media IDs")]
		static void UpdateMediaIDsForAllKoreography()
		{
			// Find all WwiseKoreographySet assets in the Asset Database.
			string[] guids = AssetDatabase.FindAssets("t:WwiseKoreographySet");

			int numSets = 0;
			int numIDs = 0;

			for (int i = 0; i < guids.Length; ++i)
			{
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);

				WwiseKoreographySet koreoSet = AssetDatabase.LoadAssetAtPath(path, typeof(WwiseKoreographySet)) as WwiseKoreographySet;

				List<WwiseKoreoMediaIDEntry> entries = koreoSet.koreographies;

                Debug.Log("entries: " + entries.Count);

				for (int j = 0; j < entries.Count; ++j)
				{
					WwiseKoreoMediaIDEntry entry = entries[j];
					Debug.Log(entry.koreo.SourceClipName);
					entry.MediaID = GetMediaIDForAudioFile(entry.koreo.SourceClipName);

					numIDs++;
				}

				numSets++;
			}

			// Save the assets to disk.
			AssetDatabase.SaveAssets();

			Debug.Log("Updated a total of [" + numIDs + "] MediaIDs from a total of [" + numSets + "] WwiseKoreographySet assets.");
		}

		/// <summary>
		/// Gets the MediaID linked to the specified audio file.
		/// </summary>
		/// <returns>The MediaID for audio file if found, 0 otherwise.</returns>
		/// <param name="fileName">The file name WITHOUT the extension.</param>
		public static uint GetMediaIDForAudioFile(string fileName)
		{
			uint mediaID = 0;

//			// Get the Wwise Project path.
//			WwiseSettings Settings = WwiseSettings.LoadSettings();
//			string wwiseProjectFullPath = AkUtilities.GetFullPath(Application.dataPath, Settings.WwiseProjectPath);
//
//			string[] paths;
//			string[] platformNames;
//
//			// Get the path to the platform-specific Wwise Sound Bank folder.
//			AkUtilities.GetWwiseSoundBankDestinationFoldersByUnityPlatform(EditorUserBuildSettings.activeBuildTarget, wwiseProjectFullPath, out paths, out platformNames);
//
//			// Get the full path to the Sound Bank folder.
//			string platformSoundbankPath = Path.GetDirectoryName(wwiseProjectFullPath) + Path.DirectorySeparatorChar + paths[0];
//
//			// Create the full path to the file.
//			string soundbanksInfoFile = platformSoundbankPath +  @"SoundbanksInfo.xml";

			// Get the Wwise SoundbanksInfo file.
			string soundbanksInfoFile = Path.Combine(AkBasePathGetter.GetPlatformBasePath(), "SoundbanksInfo.xml");

			// TODO: Cache all the results for these unless the soundbanksInfoFile changed.

			mediaID = GetMediaIDFromXML(soundbanksInfoFile, "/SoundBanksInfo/StreamedFiles/File", fileName);

			// If not found there, try in the saved-in-memory stuff.
			if (mediaID == 0)
			{
				mediaID = GetMediaIDFromXML(soundbanksInfoFile, "/SoundBanksInfo/SoundBanks/SoundBank/IncludedMemoryFiles/File", fileName);
			}

			return mediaID;
		}

		static uint GetMediaIDFromXML(string soundbanksInfoFile, string xmlPath, string fileName)
		{
			uint mediaID = 0u;

			if (File.Exists(soundbanksInfoFile))
			{
				XPathDocument infoDoc = new XPathDocument(soundbanksInfoFile);
				XPathNavigator navigator = infoDoc.CreateNavigator();
				XPathNodeIterator nodeIter = navigator.Select(xmlPath);
				
				// TODO: Cache these results and only update when the file itself has changed!
				while (nodeIter.MoveNext())
				{
					string shortName = nodeIter.Current.SelectSingleNode("ShortName").Value;
					shortName = Path.GetFileNameWithoutExtension(FixPlatformPath(shortName));
					
					if (shortName == fileName)
					{
						mediaID = uint.Parse(nodeIter.Current.GetAttribute("Id", string.Empty));
						break;
					}
				}
			}
			else
			{
				Debug.LogWarning("Warning: Could not load SoundbanksInfo.xml because the file does not exist. Expected location: " + soundbanksInfoFile);
			}

			return mediaID;
		}
		
		// Fixes up issues when the path contains the directory separator character for the wrong platform.
		static string FixPlatformPath(string path)
		{
			char incorrectSeparator = (Path.DirectorySeparatorChar == '/') ? '\\' : '/';
			
			return path.Replace(incorrectSeparator, Path.DirectorySeparatorChar);
		}
	}
}

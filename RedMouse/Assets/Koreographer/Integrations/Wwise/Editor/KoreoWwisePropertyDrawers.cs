﻿//----------------------------------------------
//            	   Koreographer                 
//    Copyright © 2014-2017 Sonic Bloom, LLC    
//----------------------------------------------

using UnityEditor;
using UnityEngine;
using SonicBloom.Koreo.Players.Wwise;

namespace SonicBloom.Koreo.EditorUI.WwiseTools
{
	[CustomPropertyDrawer(typeof(WwiseKoreoMediaIDEntry))]
	public class WwiseKoreoMediaIDEntryDrawer : PropertyDrawer
	{
		static GUIContent koreoContent = new GUIContent("Koreography", "A reference to specific Koreography data.");
		static GUIContent mediaIDContent = new GUIContent("Media ID", "The Wwise MediaID for this Koreography. It is sourced " +
			"from the SoundbanksInfo.xml file that is generated during the \"Generate SoundBanks\" process. This is " +
			"necessary for Koreographer to properly identify the audio playing in the game and connect the Koreography" +
			"to it.");

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			label = EditorGUI.BeginProperty(position, label, property);
			{
				// Create field rects.
				Rect koreoRect = new Rect(position);
				koreoRect.height = EditorGUIUtility.singleLineHeight;

				Rect idRect = new Rect(koreoRect);
				idRect.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
				idRect.xMin = EditorGUIUtility.labelWidth;

				SerializedProperty koreoProp = property.FindPropertyRelative("koreo");
				SerializedProperty mediaIdProp = property.FindPropertyRelative("mediaID");

				// Koreography field.
				EditorGUI.BeginChangeCheck();
				{
					EditorGUI.PropertyField(koreoRect, koreoProp, koreoContent);
				}
				if (EditorGUI.EndChangeCheck())
				{
					Koreography koreo = koreoProp.objectReferenceValue as Koreography;

					// Update the MediaID accordingly.
					if (koreo == null)
					{
						mediaIdProp.intValue = 0;
					}
					else
					{
						// This is ugly but it works.  Conversion is a direct bit-pattern conversion so it
						//  reinterprets to a uint correctly.
						mediaIdProp.intValue = (int)WwiseIntegrationUtils.GetMediaIDForAudioFile(koreo.SourceClipName);
					}
				}

				// This is set internally.  Always disallow editing.
				EditorGUI.BeginDisabledGroup(true);
				{
					EditorGUI.PropertyField(idRect, mediaIdProp, mediaIDContent);
				}
				EditorGUI.EndDisabledGroup();
			}
			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			// We're showing two fields per entry: two lines plus some spacing.
			return (EditorGUIUtility.singleLineHeight * 2f) + EditorGUIUtility.standardVerticalSpacing;
		}
	}

	/// <summary>
	/// Simple Property Drawer that hides Scene Objects from the lookup.
	/// </summary>
	[CustomPropertyDrawer(typeof(WwiseKoreographySet))]
	public class WwiseKoreographySetDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			// Show the Object field for the main set connection, hiding Scene Objects.
			property.objectReferenceValue = EditorGUI.ObjectField(position, label, property.objectReferenceValue, typeof(WwiseKoreographySet), false);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUIUtility.singleLineHeight;
		}
	}


	// TODO: Implement the PropertyDrawer in the future. Currently this works 99% of the way, showing
	//  the contents of a WwiseKoreographySet asset object beneath the parent object. The problem is that
	//  Undo commands do not immediately repaint the Property Drawer. This is *probably* due to the fact
	//  that the SerializedObject that the PropertyDrawer cares about is NOT the one being modified in
	//  this case. As there is no modification to the PropertyDrawer's SerializedObject, it does not
	//  receive a call to Repaint itself.
//	[CustomPropertyDrawer(typeof(WwiseKoreographySet))]
//	public class WwiseKoreographySetDrawer : PropertyDrawer
//	{
//		SerializedObject serializedAssetObj = null;
//		SerializedProperty koreoListProp = null;
//
//		void CheckSerializedObject(SerializedProperty prop)
//		{
//			if (prop.objectReferenceValue == null)
//			{
//				// Clear things out if there is no value set.
//				serializedAssetObj = null;
//				koreoListProp = null;
//			}
//			else if (serializedAssetObj == null ||
//			         serializedAssetObj.targetObject != prop.objectReferenceValue)
//			{
//				// Store objects if there is a value, including a reference to the asset.
//				serializedAssetObj = new SerializedObject(prop.objectReferenceValue);
//				koreoListProp = serializedAssetObj.FindProperty("koreographies");
//				serializedAssetObj.Update();
//			}
//		}
//
//		public override void OnGUI(Rect position, SerializedProperty setProperty, GUIContent label)
//		{
//			// Do the normal property for the WwiseKoreographySet. A simple object field.
//			Rect propRect = position;
//			propRect.yMax = propRect.yMin + EditorGUIUtility.singleLineHeight;
//
//			// Show the Object field for the main set connection, hiding Scene Objects.
//			setProperty.objectReferenceValue = EditorGUI.ObjectField(propRect, label, setProperty.objectReferenceValue, typeof(WwiseKoreographySet), false);
//
//			// Validate the object.
//			CheckSerializedObject(setProperty);
//
//			// Show the asset object contents.
//			if (serializedAssetObj != null)
//			{
//				propRect.yMin = propRect.yMax;
//				propRect.yMax = position.yMax;
//
//				EditorGUI.indentLevel++;
//				{
//					propRect = EditorGUI.IndentedRect(propRect);
//
//					EditorGUI.BeginChangeCheck();
//					{
//						serializedAssetObj.Update();
//						EditorGUI.PropertyField(propRect, koreoListProp, true);
//					}
//					if (EditorGUI.EndChangeCheck())
//					{
//						serializedAssetObj.ApplyModifiedProperties();
//					}
//				}
//				EditorGUI.indentLevel--;
//			}
//		}
//
//		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//		{
//			CheckSerializedObject(property);
//
//			// Space for the base object.
//			float height = EditorGUIUtility.singleLineHeight;
//
//			if (serializedAssetObj != null)
//			{
//				// Add a line for the Koreographies list.
//				height += EditorGUIUtility.singleLineHeight;
//
//				if (koreoListProp != null && koreoListProp.isExpanded)
//				{
//					// Add space for the contents of the Koreographies list.
//					height += EditorGUI.GetPropertyHeight(koreoListProp, null, true);
//				}
//			}
//
//			return height;
//		}
//	}
}

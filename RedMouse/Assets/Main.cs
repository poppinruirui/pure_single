﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Main :  Photon.PunBehaviour
{
	// Self
	public static Main s_Instance;

	public float m_fBallPosZ = -1.0f;

    // prefab “预设件”。  游戏中的各种对象都是用预设件来实例化出来的
    public GameObject m_preGridLine;
    public GameObject m_preBallLine;   
	public GameObject m_preBall;  
	public GameObject m_preElectronicBall;  
	public GameObject m_prePlayer;
	public GameObject m_preThorn; //  刺
	public GameObject m_preRhythm; // 节奏环
	public GameObject m_preBean;   // 豆子
	public GameObject m_preEffectPreExplode; // 预爆炸
	public GameObject m_preEffectExplode;    //  爆炸 
	public GameObject m_preBlackHole; // 黑洞
	public GameObject m_preSpitBallTarget; // 吐球的目标指示

    // UI
    Text m_txtDebugInfo;
	Slider m_sliderStickSize;
	Slider m_sliderAccelerate;
    Button m_btnSpit;

    // public data
	public float m_fRebornProtectTime = 5.0f;
	public float m_fMainTriggerUnfoldTime;
	public float m_fMainTriggerPreUnfoldTime;       
	public float m_fMainTriggerBaseScale;
	public float m_fShellShrinkSpeed;
	public float m_fThornAtteuateSpeed = 0.01f; // 球身上的刺的衰减速度
	public const float BALL_MIN_SIZE = 2.0f;
	public float m_fNewBallRunSpeed;
	public float m_fNewThornRunSpeed;
	public float m_fNewExplosionBallRunSpeed;
	public float m_fNewBallRunSpeedAccelerate;
    float m_fRadius;
    public float m_fCursorSpeed;
    public float m_fBallBaseSpeed;
    public float m_fBallMaxScale;
    public Camera m_MainCam;
    public int m_nScreenWidth;
    public int m_nScreenHeight;
    public float m_fMaxTimeToSpit; // mini seconds

	Vector3 vecTemp = new Vector3 ();
    Vector3 vecTemp1 = new Vector3();
    Vector3 vecTemp2 = new Vector3();

	/// <summary>
	/// // public GameObjects
	/// </summary>
	/// 

	public GameObject m_goThornsSpit;
	public GameObject m_goThorns;
	public GameObject m_goSpores;
	public GameObject m_goBeans;
	public GameObject m_goSpitBallTargets;                    

	GameObject m_goRedMouse;
	GameObject m_goRedMouseStick;
	GameObject m_goRedMouseBase;

	GameObject m_goCursor;
	GameObject m_goCursorImage;
	
	GameObject m_goBalls;
    GameObject m_goLines;
    GameObject m_goGrid;
	    

    float m_fCurDis;
    float m_fKX, m_fKY;
    int m_nCurDir;
    float m_fCurBallsCenterX, m_fCurBallsCenterY;

	Player m_MainPlayer;
	public GameObject m_goMainPlayer;
	public GameObject _mainplayer
	{
		get { return m_goMainPlayer; }
		set { m_goMainPlayer = value; }
	}
	PhotonView m_photonView;

	bool m_bIsUsingRedMouse;
    bool m_bMouseMode = false;



    Vector3 m_vecMainCamLastPos = new Vector3();

	bool m_bMouseDown = false;
	
	Vector3 m_vecCurTarget = new Vector3();

    // spit
    float m_fTimeElapse = 0.0f;
    bool m_bSpitting = false;

	/// <summary>
	/// / 死亡相关
	/// </summary>
	bool m_bDead = false;
	bool m_bReborning = false;
	float m_fRebornTime = 2.0f;
	bool m_bMainPlayerInited = false;
	public GameObject m_uiDeadPanel;

	// Use this for initialization
	void Start ()
	{
		s_Instance = this;

		GameObject go;

		m_goRedMouse = GameObject.Find( "RedMouse" );
		m_goCursor = GameObject.Find( "Cursor" );
		m_goCursorImage = GameObject.Find( "Cursor/CursorImage" );
		m_goBalls = GameObject.Find( "Balls" );
        m_goLines = GameObject.Find("Lines");
        m_goGrid = GameObject.Find("Grid");
		m_goRedMouseStick = GameObject.Find( "RedMouse/ControlStick" );
		m_goRedMouseBase =  GameObject.Find( "RedMouse/ControlBase" );
		UpdateRadius();
		
        //// UI
        go = GameObject.Find( "UI/textDebugInfo" );
        m_txtDebugInfo = go.GetComponent<Text>();
		
		SetMouseMode( false );
		
		// [poppin test]
		m_goRedMouseBase.transform.localScale = new Vector3( 12.0f, 12.0f, 12.0f );
		UpdateRadius();
		
		
		//go = GameObject.Find( "UI/sliderStickSize" );
		//m_sliderStickSize = go.GetComponent<Slider>();
		//m_sliderStickSize.onValueChanged.AddListener(OnSliderChanged);
		
		//go = GameObject.Find( "UI/sliderAccelerate" );
		//m_sliderAccelerate = go.GetComponent<Slider>();
		//m_sliderAccelerate.onValueChanged.AddListener(OnSliderChanged_Accelerate);

        go = GameObject.Find("UI/btnSpit");
        m_btnSpit = go.GetComponent<Button>();
        //m_btnSpit.onClick.AddListener(onClickBtn_Spit); //添加点击侦听
        //m_btnSpit.OnPointerDown = onBtnDown_Spit; //添加点击侦听
        //m_btnSpit.OnPointerUp.AddListener(onBtnUp_Spit); //添加点击侦听
        
        //// end ui

		SetRedMouseVisible ( false );

		m_bIsUsingRedMouse = false;   


        //// ! Init balls and lines
        /*
		foreach (Transform child in m_goBalls.transform)
        {
            go = child.gameObject;

            m_lstBalls.Add( go );
            Ball ball = go.GetComponent<Ball>();

            ball.SetLine(CreateLine());
        }
		*/
        /*
        foreach (Transform child in m_goLines.transform)
        {
            go = child.gameObject;
            m_lstLines.Add(go);
        }
        */
        //// !end Init balls and lines


        CalcCurBallsCenter();

        m_vecMainCamLastPos = m_MainCam.transform.position;
		m_fCurCamZoomLerpStartValue = m_MainCam.orthographicSize;

        InitGrid();
		
		//SpriteRenderer sr = m_goRedMouseBase.GetComponent<SpriteRenderer>();
		//ShowDebugInfo( sr.bounds.size.x + "," + sr.bounds.size.y );
		//m_sliderStickSize.value = m_goRedMouseBase.transform.localScale.x;
		//m_sliderAccelerate.value = m_fCursorSpeed;
		Ball.InitPools();


		InitMainPlayer ();

	}

	void InitMainPlayer ()
	{
		//m_goMainPlayer = new GameObject();
		//m_goMainPlayer.name = "player_1"; // user id
		m_goMainPlayer = PhotonInstantiate( m_prePlayer );
		m_MainPlayer = m_goMainPlayer.GetComponent<Player> ();
		BeginRebornProtectedCount ();
		m_photonView = m_goMainPlayer.GetComponent<PhotonView> ();
		m_goMainPlayer.name = "MainPlayer";// + photonView.ownerId;; // user id
		m_goMainPlayer.transform.parent = m_goBalls.transform;


		//GameObject goBall = GameObject.Instantiate (m_preBall) as GameObject;
		GameObject goBall =  PhotonInstantiate( m_preBall ) ;
		goBall.transform.localPosition = new Vector3 ( 0.0f, 0.0f, m_fBallPosZ );
		
		Ball ball = goBall.GetComponent<Ball>();
		//ball.Init ();
		ball.SetId ( m_photonView.ownerId );
		ball.SetLine( CreateLine() );    
		goBall.transform.parent = m_goMainPlayer.transform;



		m_bMainPlayerInited = true;

		/*
		// poppin test
		if (m_photonView.ownerId == 1) {
			goTest = PhotonInstantiate (m_preThorn);
			goTest.transform.localScale = new Vector3 (5.0f, 5.0f, 5.0f);
			goTest.transform.position = new Vector3 (10.0f, 10.0f, 10.0f);
		}
		*/

		//if (m_photonView.ownerId == 1) {
		//	InitBlackHole ();
		//}

	}

	public GameObject goTest;


		
	public override void OnPhotonPlayerConnected( PhotonPlayer other  )  //玩家连接  
	{  
		// 废弃！统一在player.cs中去做实例化操作
		//Debug.Log("Player connected: " + other.ID + "," + other.NickName);  
		/*
		GameObject goPlayer = new GameObject();
		goPlayer.name = "player_" + other.ID;
		goPlayer.transform.parent = m_goBalls.transform;
*/
	
	}  
		
	public GameObject PhotonInstantiate( GameObject prefab )
	{
		GameObject go = PhotonInstantiate ( prefab, new Vector3( 0.0f, 0.0f, 0f ));
		return go;
	}
		
	public GameObject PhotonInstantiate( GameObject prefab, Vector3 vecPos )
	{
		return PhotonNetwork.Instantiate( prefab.name, vecPos, Quaternion.identity, 0);
	}

    public GameObject CreateLine()
    {
        GameObject goLine = GameObject.Instantiate(m_preBallLine) as GameObject;
        goLine.transform.parent = m_goLines.transform;
        return goLine;

    }

    void InitLineForBall(Ball ball)
    {

    }

    void onBtnDown_Spit( PointerEventData evt )
    {
        Debug.Log( "11111111111" );
    }

    void onBtnUp_Spit()
    {
        Debug.Log("2222222222222");
    }


    void onClickBtn_Spit()
    {
       // Spit();
    }

	void UpdateRadius()
	{
		m_fRadius = m_goRedMouseBase.transform.localScale.x / 2.0f;
	}
	
	void OnSliderChanged_Accelerate( float val )
    {  
		m_fCursorSpeed = m_sliderAccelerate.value;
	}

	
	void OnSliderChanged( float val )
    {  
		m_goRedMouseBase.transform.localScale = new Vector3( m_sliderStickSize.value, m_sliderStickSize.value, m_sliderStickSize.value );
		UpdateRadius();
    }

	void SetRedMouseVisible( bool bVisible )
	{
		m_goRedMouse.SetActive ( bVisible );
	}

	int g_nShit = 1;
	float g_fTimeCount = 0f;
	void Tick()
	{
		g_fTimeCount += Time.deltaTime;
		if (g_fTimeCount > 1.0f) {
			g_fTimeCount = 0.0f;
			//SpitRhythm ();
			g_nShit++;
			if (g_nShit >= 5) {
				g_nShit = 1;
			}
		}


	
	}

	public void SpitRhythm()
	{
		foreach (Transform child in m_goMainPlayer.transform)
		{
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball>();
			if (ball.IsNewBallRunning ()) {
				continue;
			}

			ball.SpitRhythmBall ();
		}
	}

	// Update()最坑爹的地方在于，很多事情已经发生了，它却要等到下一帧才知道
	// Update is called once per frame
	void Update () 
	{
		//BlackHolePreSpray (); // poppin test
		//BlackHoleSpray();

		Tick (); // poppin test

		RebornProtectedCount ();

		CheckIfMainPlayerDead ();
		if (m_bMainPlayerInited) {
			if (m_bDead) {
				if (m_bReborning == false) {
					Reborn ();
				}
			}
		}

		if (m_bReborning) {
			m_fRebornTime -= Time.deltaTime;
			if (m_fRebornTime <= 0.0f) {
				DoReborn ();
			}
		}


		// [poppin test]
		if (m_goMainPlayer != null) {

		}

		Pick ();	
		ProcessRedMouse ();

     //   CameraZoom();
		
		CalcCurBallsCenter();
	//	MoveCamToBallsCenter ();

		BallsFollowCursor();
		BallsFollowCursor_NoneMouseMode();

        if (m_bSpitting)
        {
			CalcSpitBallPercent ();
			ShowSpitBallTarget ();
            m_fTimeElapse += Time.deltaTime;
			
            float fPercent = m_fTimeElapse / m_fMaxTimeToSpit;
            int nPercent = (int)( fPercent * 100.0f );
            if (nPercent > 100)
            {
                nPercent = 100;
            }
			

        }
	}

	public Vector3 RandomPosWithinScreen( float posZ )
	{
		Vector3 vecScreenMin = new Vector3 ( 0.0f, 0.0f, posZ );
		Vector3 vecScreenMax = new Vector3 ( Screen.width, Screen.height, posZ );
		Vector3 vecWorldMin = Camera.main.ScreenToWorldPoint ( vecScreenMin );
		Vector3 vecWorldMax = Camera.main.ScreenToWorldPoint ( vecScreenMax );
		float posX = (float)UnityEngine.Random.Range (vecWorldMin.x, vecWorldMax.x);
		float posY = (float)UnityEngine.Random.Range (vecWorldMin.y, vecWorldMax.y);
		return ( new Vector3 ( posX, posY, posZ) );
	}

	// poppin test

	int g_nBeanCount = 0;
	int g_nBeanBaseNum = 15;

	public int GetBeanTotal()
	{
		return m_goBeans.transform.childCount;
	}

	public void GenerateBean()
	{
		//GameObject goBean = PhotonInstantiate( m_preBean, new Vector3 ( (float)UnityEngine.Random.Range(-200, 200), (float)UnityEngine.Random.Range(-200, 200) , 10.0f ) ) ;
		Vector3 pos = RandomPosWithinScreen( 0.0f );
		GameObject goBean = GameObject.Instantiate(  m_preBean );
		Bean bean = goBean.GetComponent<Bean> ();
		pos.z = 0.0f;
		goBean.transform.localPosition = pos;
		goBean.transform.transform.parent = m_goBeans.transform;
		bean._isLocal = true;
    }

	// poppin test

	int g_nThornBaseNum = 10;
	public int GetThornTotal()
	{
		return m_goThorns.transform.childCount;
	}

	public void GenerateSomeThorns()
	{
		return;
		//GameObject goThorn = PhotonInstantiate(m_preThorn, new Vector3 ( posX,  posY, 10.0f )  );
		Vector3 pos = RandomPosWithinScreen( 10.0f );
		GameObject goThorn = GameObject.Instantiate( m_preThorn );
		goThorn.transform.parent = m_goThorns.transform;
		goThorn.transform.localScale = new Vector3 ( 2.2f, 2.2f, 1.0f );
		goThorn.transform.position = pos;
		Thorn thorn = goThorn.GetComponent<Thorn> ();
		thorn._isLocal = true;
	}

	// poppin test
	public BlackHole m_BlackHole = null;
	public GameObject m_goBlackHole = null;
	public void InitBlackHole()
	{
		m_goBlackHole = PhotonInstantiate ( m_preBlackHole, new Vector3( 20.0f, 20.0f, 10.0f ) );
		//goBlackHole.transform.localScale = new Vector3 ( 2.0f, 2.0f, 1.0f );
		m_BlackHole = m_goBlackHole.GetComponent<BlackHole>();
	}

	int m_nBlackHoleStatus = 0;
	float m_fBlackHoleRotateSpeed = 20.0f;
	float m_fBlackHolePreSprayTime = 5.0f;
	public void BlackHolePreSpray()
	{
		if (m_nBlackHoleStatus != 1) {
			return;
		}


		m_goBlackHole.transform.Rotate ( 0.0f, 0.0f, -Time.deltaTime * m_fBlackHoleRotateSpeed );
		m_fBlackHoleRotateSpeed += Time.deltaTime * 20.0f;

		m_fBlackHolePreSprayTime -= Time.deltaTime;
		if (m_fBlackHolePreSprayTime <= 0.0f) {
			m_nBlackHoleStatus = 2;
		}
	}

	float m_fCurAngel = 0.0f;
	float m_fCurAngel2 = Mathf.PI / 2.0f;
	float m_fCurAngel3 = Mathf.PI;
	float m_fCurAngel4 = Mathf.PI * 1.5f;
	float m_fSprayInterval = 0.05f;
	public void BlackHoleSpray()
	{
		if (m_nBlackHoleStatus != 2) {
			return;
		}

		m_goBlackHole.transform.Rotate ( 0.0f, 0.0f, -Time.deltaTime * m_fBlackHoleRotateSpeed );
		m_fBlackHoleRotateSpeed += Time.deltaTime * 20.0f;


		m_fSprayInterval -= Time.deltaTime;

		if (m_fSprayInterval <= 0.0f) {
			m_fSprayInterval = 0.05f;
		}
	}


    void FixedUpdate()
    {
        MoveCursor();
		MoveCursor_NoneMouseMode();
        
       // DrawLines();

		// 实时判断场景中的豆子数，如果少于规定值，则及时生成新的豆子，以保证场景中的豆子密度相对恒定

		if ( GetBeanTotal() < g_nBeanBaseNum) {
			GenerateBean ();

		}
	
		if (GetThornTotal() < g_nThornBaseNum)
		{
			GenerateSomeThorns();
		}
    }
	
	void ProcessNoneMouseMode()
	{
		if ( !m_bMouseDown )
		{
			return;
		}	

		UpdateTarget();		

	}
	
	void UpdateTarget()
	{
		m_vecCurTarget = Camera.main.ScreenToWorldPoint( m_vecMousePosition ); 
		m_vecCurTarget.z = m_fBallPosZ;
	}
	
	void ProcessMouseMode()
	{
		if (Input.GetMouseButton (0)) // 鼠标左键按下 
		{ 
			SetRedMouseVisible ( true );
			if ( m_bIsUsingRedMouse == false )
			{
				m_bIsUsingRedMouse = true;

				m_vLastPickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				m_vLastPickPos.z = -8;   
				m_goRedMouse.transform.position = m_vLastPickPos;
				m_goRedMouseStick.transform.localPosition = new Vector3( 0.0f, 0.0f, 0.0f );
                SetLinesVisible(true);
			}
		} 
		else 
		{
			if (m_bIsUsingRedMouse == true)
			{
				m_bIsUsingRedMouse = false;
				SetRedMouseVisible (false);
                //SetLinesVisible(false);
			}
		}
		
	}
	
	Vector3 m_vecMousePosition = new Vector3();
	Vector3 vecTempMousePos = new Vector3();
    void Pick()
	{
		if (/*!bOnUI && */Input.GetMouseButton (0)) 
		{
			m_bMouseDown = true;
		}
		else
		{
			m_bMouseDown = false;
		}

	
		if (Input.touchCount > 0) {
			if (Input.touchCount == 1) {
				//ShowDebugInfo (Input.touchCount + "," + nOnUiCount + "," + Input.touches [0].position );
			}
			else if  (Input.touchCount == 2) {
				// ShowDebugInfo (Input.touchCount + "," + nOnUiCount + "," + Input.touches [0].position + "," + Input.touches [1].position);

			}
				
			for ( int i = Input.touchCount -1; i >= 0; i-- ) {
				vecTempMousePos.x = Input.touches [i].position.x;
				vecTempMousePos.y = Input.touches [i].position.y;
				vecTempMousePos.z = 0.0f;
				if (My_IsPointerOverGameObject (vecTempMousePos)) {
					continue;
				}
				m_vecMousePosition = vecTempMousePos;
				m_bMouseDown = true;
				ShowDebugInfo (m_vecMousePosition + "");
			}
		}	



		// for PC
		if ( Input.GetMouseButton (0) && Input.touchCount == 0 && (!My_IsPointerOverGameObject( Input.mousePosition ) )) 
		{
			m_vecMousePosition = Input.mousePosition;
		}


		if ( m_bMouseMode )
		{
				ProcessMouseMode();	

		}
		else
		{
				ProcessNoneMouseMode ();
	
		}
		
		//CheckIfMouseDown();
		
		/*
		if (Input.GetMouseButton (0)) 
		{ // 鼠标左键按下
			if ( m_bIsUsingRedMouse == false )
			{
				m_bIsUsingRedMouse = true;
				SetRedMouseVisible ( true );
				vecTemp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                            
				vecTemp.z = 0;
				m_goRedMouse.transform.position = vecTemp;
                vecTemp.x = vecTemp.y = vecTemp.z = 0;
                m_goRedMouseStick.transform.localPosition = vecTemp;
                SetLinesVisible(true);
			}
		} 
		else 
		{
			if (m_bIsUsingRedMouse == true)
			{
				m_bIsUsingRedMouse = false;
				SetRedMouseVisible (false);
                SetLinesVisible(false);
			}
		}
		*/
	}
	
	bool CheckIfMouseDown()
	{
		if (Input.GetMouseButton (0)) 
		{
			if ( !m_bMouseDown )
			{
				UpdateTarget();	
			}
			
			m_bMouseDown = true;
		}
		else
		{
			m_bMouseDown = false;
		}
		
		return m_bMouseDown;
	}

	//// ! 知识点：千万不要去设置父容器的Scale，如果需要改变大小，就改变每个物体本身的Scale。不然会出很多诡异Bug的。
	
	void ProcessRedMouse()
	{
		if (!m_bIsUsingRedMouse) 
		{
			return;
		}
		
		if ( !m_bMouseMode )
		{
			return;
		}

		vecTemp = Camera.main.ScreenToWorldPoint(/*Input.mousePosition*/m_vecMousePosition);
		vecTemp1 = vecTemp - GetCircleCenterPos();

		vecTemp1.z = -1;
        float stickX = vecTemp1.x, stickY = vecTemp1.y;
        float fShit = Mathf.Sqrt(stickX * stickX + stickY * stickY);
        if (fShit == 0)
        {
            return;
        }
        float kx = Mathf.Abs(stickX) / fShit;
        float ky = Mathf.Abs(stickY) / fShit;
        m_fKX = kx;
        m_fKY = ky;
        float fDis = Mathf.Sqrt(vecTemp1.x * vecTemp1.x + vecTemp1.y * vecTemp1.y);
        m_fCurDis = fDis;
      //  MoveCursor(vecTemp.x, vecTemp.y, fDis);

 
            if (stickX >= 0 && stickY >= 0)
            {
                vecTemp2.x = m_fRadius * kx;
                vecTemp2.y = m_fRadius * ky;
                m_nCurDir = 1;
            }
            else if (stickX <= 0  && stickY >= 0)
            {
                vecTemp2.x = -m_fRadius * kx;
                vecTemp2.y = m_fRadius * ky;
                m_nCurDir = 2;
            }
            else if (stickX <= 0 && stickY <= 0)
            {
                vecTemp2.x = -m_fRadius * kx;
                vecTemp2.y = -m_fRadius * ky;
                m_nCurDir = 3;
            }
            else if (stickX >= 0 && stickY <= 0)
            {
                vecTemp2.x = m_fRadius * kx;
                vecTemp2.y = -m_fRadius * ky;
                m_nCurDir = 4;
            }
			


            if ( fDis >= m_fRadius ) 
            {
                m_goRedMouseStick.transform.localPosition = vecTemp2;
            }
            else
            {
                m_goRedMouseStick.transform.localPosition = vecTemp1;
            }


          
        //    Debug.Log( "cursor view pos:" + vecTemp.x + "," + vecTemp.y );
      

        
	}
	
	void MoveCursor_NoneMouseMode()
	{
		if ( m_bMouseMode )
		{
			return;
		}
		
		if ( !m_bMouseDown )
		{
			return;
		}
		
        m_goCursor.transform.position = Camera.main.ScreenToWorldPoint(m_vecMousePosition/*Input.mousePosition*/);
	
	}

	Vector3 m_vLastPickPos = new Vector3();
	void MoveCursor()
	{
		if (!m_bIsUsingRedMouse)
		{
			return;
		}

		Vector3 vecCurPickPos = Camera.main.ScreenToWorldPoint (m_vecMousePosition);
		Vector3 vecTemp1 = vecCurPickPos - m_vLastPickPos;
		Vector3 vecTemp2 = m_goCursor.transform.position;
		vecTemp2 += vecTemp1 * 2;
		m_goCursor.transform.position = vecTemp2;
		m_vLastPickPos = vecCurPickPos;
	}

	/*
    void MoveCursor()
    {
        if (!m_bIsUsingRedMouse)
        {
            return;
        }

        float fForce = m_fCurDis / m_fRadius; // 力度系数
        if ( fForce > 1 )
        {
            fForce = 1;
            fForce = 1;
        }

        vecTemp = m_goCursor.transform.position;
        float fSpeed = fForce * m_fCursorSpeed;
        switch (m_nCurDir)
        {
            case 1:
                {
                    vecTemp.x += fSpeed * m_fKX;
                    vecTemp.y += fSpeed * m_fKY;
                }
                break;
            case 2:
                {
                    vecTemp.x -= fSpeed * m_fKX;
                    vecTemp.y += fSpeed * m_fKY;

                }
                break;
            case 3:
                {
                    vecTemp.x -= fSpeed * m_fKX;
                    vecTemp.y -= fSpeed * m_fKY;

                }
                break;
            case 4:
                {
                    vecTemp.x += fSpeed * m_fKX;
                    vecTemp.y -= fSpeed * m_fKY;

                }
                break;

        } // end switch
        vecTemp.z = 0;

        //// Clamp
        vecTemp = Camera.main.WorldToScreenPoint(vecTemp);
        
        
        if (vecTemp.x > Screen.width)
        {
            vecTemp.x = Screen.width;
        }

        if (vecTemp.x < 0)
        {
            vecTemp.x = 0;
        }

        if (vecTemp.y > Screen.height)
        {
            vecTemp.y = Screen.height;
        }

        if (vecTemp.y < 0)
        {
            vecTemp.y = 0;
        }
        vecTemp = Camera.main.ScreenToWorldPoint(vecTemp);
       
        //// end clamp


        m_goCursor.transform.position = vecTemp;
    }
	*/

	void MoveTo( Camera cam, float fDestX, float fDestY, float fBaseSpeed )
	{
	    float fSrcX = cam.transform.position.x;
        float fSrcY = cam.transform.position.y;
        float fDeltaX = fDestX - fSrcX;
        float fDeltaY = fDestY - fSrcY;
        float fShit = Mathf.Sqrt(fDeltaX * fDeltaX + fDeltaY * fDeltaY);
		if ( fShit < 0.5f  )
		{
			return;
		}

        float fKX = fDeltaX / fShit;
        float fKY = fDeltaY / fShit;
        vecTemp = cam.transform.position;
        float fSpeed = fBaseSpeed;
		vecTemp.x += fSpeed * fKX * Time.deltaTime;
		vecTemp.y += fSpeed * fKY * Time.deltaTime;

        vecTemp.z = cam.transform.position.z;
        cam.transform.position = vecTemp;
		
	}

	public float CalculateBallSpeed( Ball ball )
	{
		//return m_fBallBaseSpeed * m_fBallMaxScale /  ( 2 + ball.gameObject.transform.localScale.x ) ; // 这个规则还要调的
		if (ball == null || ball._ba == null ) {
			return 0.0f;

		}

		return ball._ba._ball_realtime_velocity;
	}

	void MoveTo( GameObject go, float fDestX, float fDestY, float fBaseSpeed, ref float fKX, ref float fKY)
	{
		Ball ball = go.GetComponent<Ball> ();
		if (ball == null || ball.outter == null) {
			return;
		}

		float fSrcX = ball.outter.transform.position.x; // go.transform.position.x;
		float fSrcY = ball.outter.transform.position.y; // go.transform.position.y;
        float fDeltaX = fDestX - fSrcX;
        float fDeltaY = fDestY - fSrcY;
        float fShit = Mathf.Sqrt(fDeltaX * fDeltaX + fDeltaY * fDeltaY);
		if ( fShit == 0  )
		{
			return;
		}
        fKX = fDeltaX / fShit;
        fKY = fDeltaY / fShit;
        vecTemp = go.transform.position;
		float fSpeed = ball.speed; //fBaseSpeed * m_fBallMaxScale /  ( 2 + go.transform.localScale.x ) ;

		float fSpeedX = fSpeed * fKX;
		float fSpeedY = fSpeed * fKY;

		vecTemp.x += fSpeedX * Time.deltaTime;
		vecTemp.y += fSpeedY * Time.deltaTime;

		//ball.speed = fSpeed; // 0.02是fixedUpdate的时间间隔，单位“秒”。这个时间间隔是可以配置的

		//ball.SetMoveDir(fKX, fKY);

		vecTemp.z = go.transform.localPosition.z;
        go.transform.position = vecTemp;
		
	}


	/// <summary>
	/// / 
	/// </summary>
	/// <param name="go">Go.</param>
	/// <param name="fDestX">F destination x.</param>
	/// <param name="fDestY">F destination y.</param>
    /*
	void MoveTo(GameObject go, float fDestX, float fDestY)
    {
        Ball ball = go.GetComponent<Ball>();	
		if (ball == null)
		{
			return;
		}
		
        float fSrcX = go.transform.position.x;
        float fSrcY = go.transform.position.y;
        float fDeltaX = fDestX - fSrcX;
        float fDeltaY = fDestY - fSrcY;
        float fShit = Mathf.Sqrt(fDeltaX * fDeltaX + fDeltaY * fDeltaY);
		if ( fShit == 0  )
		{
			return;
		}
        float fKX = fDeltaX / fShit;
        float fKY = fDeltaY / fShit;
        vecTemp = go.transform.position;
        float fSpeed = m_fBallBaseSpeed * m_fBallMaxScale /  ( 2 + go.transform.localScale.x ) ;

		if (ball != null) {
			ball.speed = fSpeed;
		}
        vecTemp.x += fSpeed * fKX;
        vecTemp.y += fSpeed * fKY;

		ball.SetMoveDir(fKX, fKY);

        vecTemp.z = 0;
        go.transform.position = vecTemp;
       
    }
	*/
	public void SetMouseMode( bool bMode )
	{
		m_bMouseMode = bMode;
        if (m_bMouseMode)
        {
			m_goCursorImage.SetActive(true);
        }
        else
        {
			m_goCursorImage.SetActive(false);
			SetRedMouseVisible( false );
        }	
	}
	
    public void ToggleMouseMode()
    {
        m_bMouseMode = !m_bMouseMode;
        SetMouseMode( m_bMouseMode );
    }

    void BallsFollowCursor_NoneMouseMode()
    {
		return;

        if (m_bMouseMode)
        {
            return;
        }
		
		if ( !m_bMouseDown )
		{
		//	return;
		}

		if( m_goMainPlayer == null )
		{
			return;
		}

        foreach (Transform child in m_goMainPlayer.transform)
        {
            GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball>();
			if (ball.IsNewBallRunning ()) {
				continue;
			}
			float fKX = 0.0f, fKY = 0.0f;
			MoveTo(go, m_vecCurTarget.x, m_vecCurTarget.y, m_fBallBaseSpeed, ref fKX, ref fKY); // right here
			ball.SetMoveDir( fKX, fKY );
        }
    }


    void BallsFollowCursor()
    {
        if (!m_bIsUsingRedMouse)
        {
            //return;
        }

        if (!m_bMouseMode)
        {
            return;
        }

		if (m_goMainPlayer == null) {
			return;
		}

		foreach (Transform child in m_goMainPlayer.transform)
        {
            GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball>();

			if (ball == null) {
				continue;
			}

			if (ball.IsNewBallRunning ()) {
				continue;
			}   
			float fKX = 0.0f, fKY = 0.0f;
			MoveTo(go, m_goCursor.transform.position.x, m_goCursor.transform.position.y, m_fBallBaseSpeed, ref fKX, ref fKY);
			ball.SetMoveDir( fKX, fKY );
		
        }
    }

    void SetLinesVisible( bool bVisible )
    {
        m_goLines.SetActive( bVisible );
    }

    void DrawLines()
    {
        GameObject goLine, goBall, goOutter;
		if (m_goMainPlayer == null)
		{
			return;
		}

        foreach ( Transform child in m_goMainPlayer.transform  )
        {
            goBall = child.gameObject;
            Ball ball = goBall.GetComponent<Ball>();
			goOutter = ball.outter;
            goLine = ball.GetLine();
			if (goLine == null) {
				continue;
			}
			goLine.SetActive( true );
            LineRenderer lr = goLine.GetComponent<LineRenderer>();
			if( lr == null || goOutter == null )
			{
				continue;
			}
			lr.SetPosition( 0, /*goBall.transform.position +*/ goOutter.transform.position );
            lr.SetPosition( 1, m_goCursor.transform.position);
        }


    }

	void LeaveRoom()
	{
		PhotonNetwork.LeaveRoom();
	}


	float m_fRebornProtectedCountTime = 5.0f;
	bool m_bRebornProtectedCounting = false;
	void Reborn()
	{
		m_bMainPlayerInited = false;
		m_bReborning = true;
		m_uiDeadPanel.SetActive ( true );
		m_fRebornTime = 2.0f;

		//m_MainPlayer.SetRebornProtected ( true );
	}

	void DoReborn()
	{
		GameObject goBall =  PhotonInstantiate( m_preBall ) ;
		Ball ball = goBall.GetComponent<Ball>();
		ball.SetId ( m_photonView.ownerId );
		ball.SetLine( CreateLine() );    
		goBall.transform.parent = m_goMainPlayer.transform;  
		m_uiDeadPanel.SetActive ( false );
		m_bReborning = false;
		m_bDead = false;
		m_bMainPlayerInited = true;
		BeginRebornProtectedCount ();
	}

	public void BeginRebornProtectedCount()
	{
		m_fRebornProtectedCountTime = 5.0f;
		m_bRebornProtectedCounting = true;
		m_MainPlayer.SetRebornProtected ( true );
	}

	public void RebornProtectedCount()
	{
		if (!m_bRebornProtectedCounting) {
			return;
		}

		m_fRebornProtectedCountTime -= Time.deltaTime;
		if (m_fRebornProtectedCountTime <= 0.0f) {
			EndRebornProtectedCount ();
		}
	}

	public void EndRebornProtectedCount()
	{
		m_bRebornProtectedCounting = false;
		m_MainPlayer.SetRebornProtected ( false );

	}

	void CheckIfMainPlayerDead ()
	{
		int nCount = 0;

		if (m_goMainPlayer == null) {
			m_bDead = true;
			return;
		}

		foreach (Transform child in m_goMainPlayer.transform) 
		{
			nCount++;
		}

		if (nCount == 0) {
			m_bDead = true;
		} else {
			m_bDead = false;
		}
	}

    void CalcCurBallsCenter()
    {
		if(m_bDead )
		{
			return;
		}

		if (m_goMainPlayer == null) {
			return;
		}

        float fSumX = 0, fSumY = 0;
		int nCount = 0;
		foreach ( Transform child in m_goMainPlayer.transform  )
        {
            GameObject goBall = child.gameObject;
			//GameObject goOutter = goBall.transform.Find( "OutterBall" ).gameObject;
			Ball ball = goBall.GetComponent<Ball>();
			GameObject goOutter = ball.outter;
			fSumX += goOutter.transform.position.x;
			fSumY += goOutter.transform.position.y;
			nCount++;
        }

        m_fCurBallsCenterX = fSumX / nCount;
        m_fCurBallsCenterY = fSumY / nCount;
    }

	public float m_fCamSpeed = 0.01f;
	void MoveCamToBallsCenter() // right here
	{
		if (m_bDead) {
			return;
		}

        vecTemp = m_MainCam.transform.position;
        vecTemp.x = m_fCurBallsCenterX;
        vecTemp.y = m_fCurBallsCenterY;
		//m_MainCam.transform.position = vecTemp;
		// 要做插值，不能直接跟随这群球的中心点，否则一旦遇到某个球或某些球被吃掉，中心点位置剧烈变化，相机跳跃感很严重，玩家感官上很不爽
		MoveTo (m_MainCam, m_fCurBallsCenterX, m_fCurBallsCenterY, m_fCamSpeed);

		
		//m_MainCam.transform.position = vecTemp;	

		DoSomethingDueToMainCamMove(m_goRedMouse);
		DoSomethingDueToMainCamMove(m_goCursor); 
		DoSomethingDueToMainCamMove(ref m_vecCurTarget );

        m_vecMainCamLastPos = m_MainCam.transform.position;
	}

	void DoSomethingDueToMainCamMove( ref Vector3 vecTarget )
	{
        float fDeltaX = m_MainCam.transform.position.x - m_vecMainCamLastPos.x;
        float fDeltaY = m_MainCam.transform.position.y - m_vecMainCamLastPos.y;
		vecTarget.x += fDeltaX;
        vecTarget.y += fDeltaY;
	}

    void DoSomethingDueToMainCamMove( GameObject go )
    {
        float fDeltaX = m_MainCam.transform.position.x - m_vecMainCamLastPos.x;
        float fDeltaY = m_MainCam.transform.position.y - m_vecMainCamLastPos.y;
		vecTemp = go.transform.position;
		vecTemp.x += fDeltaX;
        vecTemp.y += fDeltaY;
		go.transform.position = vecTemp;
    }

    Vector3 GetCircleCenterPos()
    {
        return m_goRedMouse.transform.position;
    }


    void InitGrid()
    {
		return;

		int nInterval = 5;
        int nCount = 50;
		float fZ = -0.1f;
        for (int i = -nCount; i < nCount; i++)
        {
            GameObject line = GameObject.Instantiate(m_preGridLine) as GameObject;
            line.transform.parent = m_goGrid.transform;
            LineRenderer lr = line.GetComponent<LineRenderer>();
			lr.SetPosition(0, new Vector3( -1000, i * nInterval, fZ ) );
			lr.SetPosition(1, new Vector3(  1000, i * nInterval, fZ) );
        } // end for i 

        for (int i = -nCount; i < nCount; i++)
        {
            GameObject line = GameObject.Instantiate(m_preGridLine) as GameObject;
            line.transform.parent = m_goGrid.transform;
            LineRenderer lr = line.GetComponent<LineRenderer>();
			lr.SetPosition(0, new Vector3(i * nInterval, -1000, fZ));
			lr.SetPosition(1, new Vector3(i * nInterval, 1000, fZ));
        } // end for i 

    }

    void OnPostRender()
    {
     //   DrawGrid();
    }

    void ShowDebugInfo(string szInfo)
    {
        m_txtDebugInfo.text = szInfo;
    }

    List<float> lstPosX = new List<float>();
    List<float> lstPosY = new List<float>();

    void BubbleSort(ref List<float> lst)
    {
        float goTemp = 0.0f;
        for (int i = 0; i < lst.Count - 1; i++)
        {
            for (int j = i + 1; j < lst.Count; j++)
            {
                if (lst[i] > lst[j])
                {
                    goTemp = lst[i];
                    lst[i] = lst[j];
                    lst[j] = goTemp;
                }

            } // end for j  
        } //end for i 
    }

	float m_fCameraZoomTime = 0.0f; 
	float m_fCurCamZoomLerpStartValue = 0.0f;
	float m_fTargetZoomValue = 0.0f;
	float m_fLerpTotalTime = 1.0f;
	float m_fCameraZoomSpeed = 0.0f;
	void StartCameraZoomLerp()
	{
		m_fCameraZoomTime = 0.0f;
		m_fCurCamZoomLerpStartValue = m_MainCam.orthographicSize;
	}



	void DoCameraZoomLerp()
	{
		m_fCameraZoomTime += Time.deltaTime;
		if (m_fCameraZoomTime > m_fLerpTotalTime) {
			m_fCameraZoomTime = m_fLerpTotalTime;
		}
		float t = m_fCameraZoomTime / m_fLerpTotalTime;
		float fSize =  Mathf.Lerp ( m_fCurCamZoomLerpStartValue, m_fTargetZoomValue, t );
		if (fSize > 45.0f) { // poppin test
			fSize = 45.0f;
		}
		m_MainCam.orthographicSize = fSize;

	}

    void CameraZoom() 
    {
        lstPosX.Clear();
        lstPosY.Clear();

		if( m_goMainPlayer == null )
		{
			return;
		}

		float fMaxLeft = 0.0f, fMaxRight = 0.0f, fMaxTop = 0.0f, fMaxBottom = 0.0f;
		bool bFirst = true;
		foreach ( Transform child in m_goMainPlayer.transform )
        {
			Ball ball = child.gameObject.GetComponent<Ball>();
			SpriteRenderer sr = ball.outter.GetComponent<SpriteRenderer> ();
			float fLeft = ball.outter.transform.position.x - sr.bounds.size.x / 2.0f;
			float fRight = ball.outter.transform.position.x + sr.bounds.size.x / 2.0f;
			float fTop = ball.outter.transform.position.y + sr.bounds.size.y / 2.0f;
			float fBottom = ball.outter.transform.position.y - sr.bounds.size.y / 2.0f;

			if (bFirst) {
				fMaxLeft = fLeft;
				fMaxRight = fRight;
				fMaxTop = fTop;
				fMaxBottom = fBottom;
				bFirst = false;
			} else {
				if (fLeft < fMaxLeft) {
					fMaxLeft = fLeft;
				}

				if (fRight > fMaxRight) {
					fMaxRight = fRight;
				}

				if (fTop > fMaxTop) {
					fMaxTop = fTop;
				}

				if (fBottom < fMaxBottom) {
					fMaxBottom = fBottom;
				}
			}
        }
	
		float fSize = ( fMaxRight - fMaxLeft ) + ( fMaxTop - fMaxBottom ) * 1.5f;

	//	m_MainCam.orthographicSize = 

        //m_MainCam.orthographicSize = 20 + fMax * 0.3f;
	
		float fTargetValue =  fSize;
	//	Debug.Log ( "fTargetValue= " + fMaxLeft + " , " + fMaxRight + " , " + fMaxBottom + " , " +  fMaxTop );
		if (m_fTargetZoomValue != fTargetValue) {
			m_fTargetZoomValue = fTargetValue;
			StartCameraZoomLerp ();
		}
		DoCameraZoomLerp ();

    }

	// 判断当前是否点击在了UI上
	public bool My_IsPointerOverGameObject( Vector3 vecMousePosition )
    {
        //if (Input.touchCount > 0) {  

        //    int id = Input.GetTouch(0).fingerId;  
        //    return UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(id);//安卓机上不行  
        //}  
        //else {  
        //return UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();  
        PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
		eventData.pressPosition = vecMousePosition/*Input.mousePosition*/;
		eventData.position = vecMousePosition/*Input.mousePosition*/;

        List<RaycastResult> list = new List<RaycastResult>();
        UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

        return list.Count > 0;

    }

	/// <summary>
	/// 吐球
	/// </summary>
    List<GameObject> m_lstTemp = new List<GameObject>();

	Ball SpitBall( Ball ball, float fChildRadius )
	{
		Ball new_ball = null;

		if (fChildRadius < Main.BALL_MIN_SIZE )
		{
			return null;
		}

		float fMotherCurRadius = 0.0f;
		if (!ball.CheckIfHaveTheSmallestSizeToSpitBall ( ref fMotherCurRadius )) {
			return null;
		}
			
		float fMotherLastRadius = ball.GetSize(); //  再次强调：在unity中设置scale，控制的是圆的半径尺寸而不是面积
	
		ball.SetSize(fMotherCurRadius);
	
		GameObject goNewBall = PhotonNetwork.Instantiate( m_preBall.name, new Vector3(0f,0f,0f), Quaternion.identity, 0);
		goNewBall.transform.localPosition = new Vector3 (0f, 0f, m_fBallPosZ);
		new_ball = goNewBall.GetComponent<Ball>();
		if (new_ball == null)
		{
			Debug.LogError ( "new_ball null!!!!!!!" );
			return null;
		}

		/*
		float fCurChildArea = fChildRadius * fChildRadius;
		float fCurMotherArea = fMotherCurRadius * fMotherCurRadius;
		float fTotalArea = fCurChildArea + fCurMotherArea;
		float fCurChildAreaPercent = fCurChildArea / fTotalArea;
		float fCurMotherAreaPercent = 1.0f - fCurChildAreaPercent;
		*/

		//new_ball.Init ();
		new_ball.SetSize(fChildRadius);

		ball.CalculateNewBallBornPosAndRunDire ( new_ball );

		new_ball.BeginNewBallRun( Main.s_Instance.m_fNewBallRunSpeed );


		return new_ball;
	}

    void Spit(float fPercent)
    {
		if (!m_photonView.isMine) {
			return;
		}


        m_lstTemp.Clear();

		foreach ( Transform child in m_goMainPlayer.transform )	
		{
	            Ball ball = child.gameObject.GetComponent<Ball>();

			if (ball.IsNewBallRunning ()) {
				continue;
			}

			if (!ball.CheckIfCanSpitBall ()) {
				continue;
			}

			//Ball new_ball = ball.Spit(fPercent, PhotonInstantiate( m_preBall ));
			Ball new_ball = null;

			float fMotherLastRadius = ball.GetSize(); //  再次强调：在unity中设置scale，控制的是圆的半径尺寸而不是面积大小
			float fChildRadius = Mathf.Sqrt(fPercent) * fMotherLastRadius; // percent是被限制死了的，最多50%

			if (fChildRadius < Main.BALL_MIN_SIZE )
			{
				fChildRadius = Main.BALL_MIN_SIZE;
			}

			float fMotherCurRadius = ball.CalculateMotherLeftSize( fChildRadius );

			if (fChildRadius < Main.BALL_MIN_SIZE || fMotherCurRadius < Main.BALL_MIN_SIZE)
			{
				continue;
			}

	

			ball.SetSize(fMotherCurRadius);
			ball.SetInnerBallSize( 0.5f ); 

			//// Clone
			//GameObject goNewBall = GameObject.Instantiate(this.gameObject) as GameObject;
			GameObject goNewBall = PhotonNetwork.Instantiate( m_preBall.name, new Vector3(0f,0f,0f), Quaternion.identity, 0);
			goNewBall.transform.localPosition = new Vector3 (0f, 0f, m_fBallPosZ);
			new_ball = goNewBall.GetComponent<Ball>();
			if (new_ball == null)
			{
				Debug.LogError ( "new_ball null!!!!!!!" );
				continue;
			}

			float fCurChildArea = fChildRadius * fChildRadius;
			float fCurMotherArea = fMotherCurRadius * fMotherCurRadius;
			float fTotalArea = fCurChildArea + fCurMotherArea;
			float fCurChildAreaPercent = fCurChildArea / fTotalArea;
			float fCurMotherAreaPercent = 1.0f - fCurChildAreaPercent;


			//new_ball.Init ();
			new_ball.SetSize(fChildRadius);
			new_ball.SetInnerBallSize( 0.5f );

			new_ball.SetCaster ( ball );

			//Debug.Log ( "分了分了：" + fMotherCurRadius + " , " + fChildRadius );
			float fMotherLeftSize = 0.0f;
			if (!new_ball.CheckIfHaveTheSmallestSizeToSpitBall( ref fMotherLeftSize )) { // 分出来的球不具备再分的最低尺寸

			} else { // 分出来的球达到了再分的最低要求尺寸
				float fMotherLastThornSize = ball.GetCurThornTotalSize();
				float fChildCurThornSize = fMotherLastThornSize * Mathf.Sqrt( fCurChildAreaPercent );
				float fMotherCurThornSize = fMotherLastThornSize * Mathf.Sqrt( fCurMotherAreaPercent );
				ball.SetCurThornTotalSize ( fMotherCurThornSize );
				new_ball.SetCurThornTotalSize( fChildCurThornSize );
			}

			ball.SetName ("mother");
			new_ball.SetName ( "Child" );

			ball.CalculateNewBallBornPosAndRunDire ( new_ball );

			/*
			SpriteRenderer srMother = ball.outter.GetComponent<SpriteRenderer>();
			SpriteRenderer srChild = new_ball.outter.GetComponent<SpriteRenderer>();

			vecTemp = ball.outter.gameObject.transform.position;;//ball.gameObject.transform.position;
			float fMotherCurRadiusAbsVal = srMother.bounds.size.x  / 2.0f;
			float fChildCurRadiusAbsVal = srChild.bounds.size.x  / 2.0f;
			float fDis = fMotherCurRadiusAbsVal +  fChildCurRadiusAbsVal;
			float fKX = 0.0f, fKY = 0.0f;
			ball.GetMoveDir (ref fKX, ref fKY);
			vecTemp.x += fDis * fKX;
			vecTemp.y += fDis * fKY; 
			new_ball.SetMoveDir ( fKX, fKY );
			goNewBall.transform.position = vecTemp;
			*/

			new_ball.BeginNewBallRun( Main.s_Instance.m_fNewBallRunSpeed );

			// end clone

            new_ball.SetLine(CreateLine());
            //CircleCollider2D collider = new_ball.GetComponent<CircleCollider2D>();
            //collider.enabled = false;

            m_lstTemp.Add(new_ball.gameObject);

			//ball.BeginInnerBallGrow ();
			ball.BeginShellShrink();
			new_ball.BeginShellShrink();
        }

        for (int i = 0; i < m_lstTemp.Count; i++)
        {
            (m_lstTemp[i] as GameObject ).transform.parent = m_goMainPlayer.transform;
        }
    }

	public void ShowSpitBallTarget ()
	{
		if (!m_bSpitting) {
			return;
		}

		foreach (Transform child in m_goMainPlayer.transform) {
			Ball ball = child.gameObject.GetComponent<Ball> ();
			ball.ProcessSpitBallTarget ( m_fSpitBallPercent );
		}
	}
    public void BeginSpit()
    {
        m_bSpitting = true;
        m_fTimeElapse = 0.0f;

    }

    public void EndSpit()
    {
        
      //  Debug.Log( "按下时间:" +  m_fTimeElapse);
        if (m_fTimeElapse > m_fMaxTimeToSpit)
        {
            m_fTimeElapse = m_fMaxTimeToSpit;
        }
		CalcSpitBallPercent ();
		float fPercent = m_fSpitBallPercent;//m_fTimeElapse / m_fMaxTimeToSpit * 0.5f;
		if (m_bSpitting == true) {
			Spit (fPercent);
		}
		m_bSpitting = false;
	}

	float m_fSpitBallPercent = 0.0f;
	public void CalcSpitBallPercent()
	{
		m_fSpitBallPercent =  m_fTimeElapse / m_fMaxTimeToSpit * 0.5f;
	}

	void InnerBallGrow()
	{
		int nCount = 0;     
		foreach (Transform child in m_goBalls.transform) 
		{
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball>();
			ball.InnerBallGrow ();
			nCount++;
		}
	}

	//// 吐刺
	public void SpitThorn()
	{
		Ball ball = null;
		foreach (Transform child in m_goMainPlayer.transform) {
			if (child.gameObject == null) {
				continue;
			}
			ball = child.gameObject.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}
			Thorn thorn = ball.SpitThorn ();

		}
	}

	// 把“刺”添加进刺的列表中
	public void AddThornToThornList( Thorn thorn )
	{
		thorn.gameObject.transform.parent = m_goThorns.transform;
	}

	float m_fIncDecMent = 0.1f;
	public void BallSizeInc()
	{
		//m_nBlackHoleStatus = 1;

		Ball ball = null;
		foreach (Transform child in m_goMainPlayer.transform) {
			if (child.gameObject == null) {
				continue;
			}
			ball = child.gameObject.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}

			float fSize = 1.0f;
			ball.SetSize (fSize );
		} // end foreach
	}

	float g_fHitTime = 0.0f;
	public void BallSizeDec()
	{
		Ball ball = null;

		foreach (Transform child in m_goMainPlayer.transform) {
			if (child.gameObject == null) {
				continue;
			}
			ball = child.gameObject.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}

			float fSize = 2.0f;//ball.GetSize () - m_fIncDecMent;
			ball.SetSize ( fSize);
		} // end foreach
	}

	// 猥琐地扩张
	public void Unfold()
	{
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}

			if (!ball.CheckIfCanUnfold ()) {
				continue;
			}

			ball.BeginPreUnfold ();

		} // end foreach		
	}

	// 吐孢子
	public void SpitSpore()
	{
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			Ball ballSpore =  SpitBall ( ball, BALL_MIN_SIZE );
			if (ballSpore) {
				ballSpore.gameObject.transform.parent = m_goSpores.transform;
				ballSpore._collider.isTrigger = true;
			}
		} // end foreach		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhythmBall : Ball {

	// Use this for initialization
	void Start () {
		
	}

	void Awake()
	{
		Init ();
	}
		

	// Update is called once per frame
	void Update () {
		DoRhythmBallRun ();

	}

	public override void Init()
	{
		GameObject go;

		Transform transOuuter = this.gameObject.transform.Find ("OutterBall"); // 规定：一个球必须有“外环”

		if (transOuuter) {
			m_goOutterBall = transOuuter.gameObject;
		}

		if ( m_goOutterBall == null)
		{
			isInited = false;
			Debug.LogError ( "木得外环怎么行" );
			return;
		}

		m_fRhythmBallRunSpeed = Main.s_Instance.m_fNewBallRunSpeed;
	}

	float m_fRhythmBallRunSpeed = 0.0f;

	public void DoRhythmBallRun()
	{
		if (m_fRhythmBallRunSpeed < 0.0f) {
			Destroy (this.gameObject);
		}

		Vector3 vecTemp = this.gameObject.transform.position;
		vecTemp.x += m_fRhythmBallRunSpeed * _kx * Time.deltaTime;
		vecTemp.y += m_fRhythmBallRunSpeed * _ky * Time.deltaTime;
		this.gameObject.transform.position = vecTemp;
		m_fRhythmBallRunSpeed += Main.s_Instance.m_fNewBallRunSpeedAccelerate;
	}
}

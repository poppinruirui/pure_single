﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thorn : Ball //  “刺”继承于“球”，尼玛刺就是球的癌细胞
{
	

	// Use this for initialization
	void Start () {
		
	}

	void Awake()
	{
		isThorn = true;
		_balltype = eBallType.ball_type_thorn;
		Init ();

	}

	// Update is called once per frame
	public override  void Update () {

		NewBallRun ();

		_trigger.transform.localScale = outter.transform.localScale;


		RefeshPosDueToOutOfScreen ();
		ShitLife ();
	}

	public override void Init()
	{
		Transform transOuuter = this.gameObject.transform.Find ("InnerFill"); 
		if (transOuuter) {
			m_goOutterBall = transOuuter.gameObject;
		}

		if ( m_goOutterBall == null)
		{
			isInited = false;
			Debug.LogError ( "木得外环怎么行" );
			return;
		}


		_collider = m_goOutterBall.GetComponent<CircleCollider2D> ();

		Transform transMainTrigger = this.gameObject.transform.Find ("MainTrigger"); 
		if ( transMainTrigger )
		{
			
			_trigger = transMainTrigger.gameObject;
			_colliderTrigger = _trigger.GetComponent<CircleCollider2D> ();
			_triggersr = _trigger.GetComponent<SpriteRenderer> ();
		}

		_ba = this.gameObject.GetComponent<BallAction> ();
	}

	float m_fLifeTime = 20.0f;
	void ShitLife()
	{
		if (!_isSpited) {
			return;
		}

		m_fLifeTime -= Time.deltaTime;
		if (m_fLifeTime < 0) {
			DestroyBall ( this );
		}
	}

}

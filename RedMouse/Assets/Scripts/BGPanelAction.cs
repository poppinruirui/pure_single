using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SonicBloom.Koreo;

public class BGPanelAction : MonoBehaviour
{
    public const string KOREO_EVENT_TRACK_SNAPSHOT = "TRACK_SNAPSHOT";
    
    public GameObject _bg_panel;    
    
    public float _random_color_interval = 5.0f;
    float _last_random_color_time = 0.0f;

    void Start()
    {
        this.RandomBackgroundColor(true);
        Koreographer.Instance.RegisterForEvents(KOREO_EVENT_TRACK_SNAPSHOT,
                                                this.OnKoreoEventTrackSnapshot);
    }

    void OnDestroy()
    {
        if (Koreographer.Instance != null) {
            Koreographer.Instance.UnregisterForAllEvents(this);
        }    
    }
    
    public void RandomBackgroundColor(bool immediately)
    {
        float now = Time.time;
        if (immediately ||
            (now - this._last_random_color_time > this._random_color_interval)) {
           float light_range = 0.0f;
           this._bg_panel.GetComponent<SpriteRenderer>().color =
               ColorPalette.RandomBackgroundColor(out light_range);
           BGLightAction.SetLightRange(light_range);
            this._last_random_color_time = now;
        }
    }
    
    void FixedUpdate()
    {
         this.RandomBackgroundColor(false);
    }

    void OnKoreoEventTrackSnapshot(KoreographyEvent koreoEvent)
    {
        this.RandomBackgroundColor(true);
    }
};

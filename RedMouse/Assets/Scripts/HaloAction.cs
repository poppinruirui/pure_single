﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using SonicBloom.Koreo;

public class HaloAction : MonoBehaviour
{
    public const string KOREO_EVENT_TRACK_BEAT_CURVE = "TRACK_BEAT_CURVE";
    
    public float _halo_spread_factor = 4.0f;
    public float _halo_intensity_base = 2.0f;    
    
	void Start()
    {
        Koreographer.Instance.RegisterForEventsWithTime(KOREO_EVENT_TRACK_BEAT_CURVE,
                                                        this.OnKoreoEventTrackBeatCurve);        
	}

    void OnDestroy()
    {
        if (Koreographer.Instance != null) {
            Koreographer.Instance.UnregisterForAllEvents(this);
        }            
    }
	
	void Update()
    {		
	}

    void OnKoreoEventTrackBeatCurve(KoreographyEvent koreoEvent,
                                    int sampleTime,
                                    int sampleDelta,
                                    DeltaSlice deltaSlice)
    {
        float value = koreoEvent.GetValueOfCurveAtTime(sampleTime);
        value += 1.0f; // [0, 2]
        Bloom bloom = this.GetComponent<Bloom>();
        bloom.sepBlurSpread = value * this._halo_spread_factor;
        bloom.bloomIntensity = this._halo_intensity_base + (2.0f - value); // [4, 2]
    }
}

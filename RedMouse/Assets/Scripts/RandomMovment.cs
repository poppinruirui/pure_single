using UnityEngine;

public class RandomMovment
{
    public float _move_velocity = 1.0f;
    public float _view_port_overflow = 0.05f;

    public Transform _transform;
    
    Vector3 _target_position = Vector3.zero;

    public void RandomMove(bool immediately, Camera camera)
    {
        if (!camera) {
            camera = Camera.main;
        }
        if (immediately ||
            Vector3.Distance(this._transform.position, this._target_position) <= 0.1f) {            
            float view_port_r = camera.rect.xMin - this._view_port_overflow;
            float view_port_l = camera.rect.xMax + this._view_port_overflow;
            float view_port_t = camera.rect.yMin - this._view_port_overflow;
            float view_port_b = camera.rect.yMax + this._view_port_overflow;
            this._target_position = 
                camera.ViewportToWorldPoint(new Vector3(Random.Range(view_port_l, view_port_r),
                                                        Random.Range(view_port_b, view_port_t),
                                                        this._transform.position.z -
                                                        camera.transform.position.z));
		
        }
        else {        
            Vector3 movement = this._target_position - this._transform.position;
            movement.Normalize();
            movement *= this._move_velocity;
            movement *= Time.fixedDeltaTime;
            movement += this._transform.position;
            this._transform.position = movement;
        }
    }
};

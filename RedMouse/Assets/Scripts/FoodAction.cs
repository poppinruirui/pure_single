using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class FoodAction : MonoBehaviour
{
    public const string KOREO_EVENT_TRACK_MEASURE_CURVE = "TRACK_MEASURE_CURVE";
    
    public float   _translate_factor = 0.003f;
    public float   _scale_factor = 0.03f;
    public Vector3 _default_scale = new Vector3(0.20f, 0.20f, 1.0f);
    
    void Start()
    {
        this.RandomColor();
        this.RandomPosition();
        Koreographer.Instance.RegisterForEventsWithTime(KOREO_EVENT_TRACK_MEASURE_CURVE,
                                                        this.OnKoreoEventTrackMeasureCurve);        
    }

    void OnDestroy()
    {
        if (Koreographer.Instance != null) {
            Koreographer.Instance.UnregisterForAllEvents(this);
        }    
    }
    
    void FixedUpdate()
    {
    }
    
    void RandomColor()
    {
        this.GetComponent<SpriteRenderer>().color = ColorPalette.RandomFoodColor();        
    }

    void RandomPosition()
    {
        Camera camera = Camera.main;
        float view_port_r = camera.rect.xMin;
        float view_port_l = camera.rect.xMax;
        float view_port_t = camera.rect.yMin;
        float view_port_b = camera.rect.yMax;
        this.transform.position =
            camera.ViewportToWorldPoint(new Vector3(Random.Range(view_port_l, view_port_r),
                                                    Random.Range(view_port_b, view_port_t),
                                                    this.transform.position.z -
                                                    camera.transform.position.z));
    }

    void OnKoreoEventTrackMeasureCurve(KoreographyEvent koreoEvent,
                                       int sampleTime,
                                       int sampleDelta,
                                       DeltaSlice deltaSlice)
    {
        float value = koreoEvent.GetValueOfCurveAtTime(sampleTime); // [0, 1]
        value -= 0.5f; // [-0.5, 0.5]
        this.transform.Translate(Vector3.right * value * this._translate_factor);
        this.transform.Rotate(Vector3.forward * value * Mathf.PI);
        this.transform.localScale = this._default_scale +
            new Vector3(value * this._scale_factor,
                        value * this._scale_factor,
                        0.0f);
    }
};

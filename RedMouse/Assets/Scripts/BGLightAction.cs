﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
         
public class BGLightAction : MonoBehaviour
{
    public float _move_velocity = 2.5f;

    public float _light_range = 25.0f;
    public float _light_duration = 3.0f;
    
    RandomMovment _random_movement = new RandomMovment();
    
	void Start()
    {
        this._random_movement._transform = this.transform;
        this._random_movement._move_velocity = this._move_velocity;
        this.RandomBackgroundLight(true);
	}

    void Update()
    {
        float amplitude = Mathf.PingPong(Time.time, this._light_duration);
        amplitude = amplitude / this._light_duration * 0.5f + 0.5f;
        this.GetComponent<Light>().range = this._light_range * amplitude;
    }

    void RandomBackgroundLight(bool immediately)
    {
        GameObject bg_camera = GameObject.Find("Camera_BG");
        this._random_movement.RandomMove(immediately, bg_camera.GetComponent<Camera>());
    }

	void FixedUpdate()
    {
        this.RandomBackgroundLight(false);
	}

    public static void SetLightRange(float range)
    {
        GameObject[] lights = GameObject.FindGameObjectsWithTag("Light");
        for (int i=0; i<lights.Length; ++i) {
            GameObject light = lights[i];
            light.GetComponent<BGLightAction>()._light_range = range;
        }
    }
}

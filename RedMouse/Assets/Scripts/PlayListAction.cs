using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SonicBloom.Koreo.Players.Wwise;

public class PlayListAction : MonoBehaviour
{
    public Text _bgm_title;
    
    string[]  _playlist_states = {
        "State_00",
        "State_01",
        "State_02",
        "State_03"
    };
    string[]  _playlist_titles = {
        "Await - KevInVoo",
        "Banggarang - Skrillex",
        "Kyoto - Skrillex",
        "Perish - Cre-sc3NT"
    };
    int _index_of_playlist = 0;

    void Start()
    {
        this.PlayBGM();
    }

    void Destroy()
    {
        this.StopBGM();
    }

    public void TriggerExplosion()
    {
        WwiseMusicVisor music_visor = this.GetComponent<WwiseMusicVisor>();
        AkSoundEngine.PostEvent("TriggerExplosion",
                                this.gameObject,
                                (uint)(AkCallbackType.AK_Duration |
                                       AkCallbackType.AK_EnableGetSourcePlayPosition |
                                       AkCallbackType.AK_EndOfEvent),
                                music_visor.HandleAKCallback,
                                null);        
    }
    
    public void PlayBGM()
    {
        WwiseMusicVisor music_visor = this.GetComponent<WwiseMusicVisor>();
		AkSoundEngine.PostEvent ("PlayBGM",
			this.gameObject,
                                (uint)(AkCallbackType.AK_Duration |
                                       AkCallbackType.AK_EnableGetSourcePlayPosition |
                                       AkCallbackType.AK_EndOfEvent),
                                music_visor.HandleAKCallback,
                                null);
        AkSoundEngine.SetState("PlaylistStatesGroup", this._playlist_states[this._index_of_playlist]);
        //this._bgm_title.text = this._playlist_titles[this._index_of_playlist];
    }

    public void StopBGM()
    {
        AkSoundEngine.PostEvent("StopBGM", this.gameObject);
    }
    
    public void NextBGM()
    {
        if (++this._index_of_playlist >= this._playlist_states.Length) {
            this._index_of_playlist = 0;
        }
        AkSoundEngine.PostEvent("TriggerSwipe", this.gameObject);
        AkSoundEngine.SetState("PlaylistStatesGroup", this._playlist_states[this._index_of_playlist]);
        this._bgm_title.text = this._playlist_titles[this._index_of_playlist];
    }

    public void PrevBGM()
    {
        if (--this._index_of_playlist < 0) {
            this._index_of_playlist = this._playlist_states.Length - 1;
        }
        AkSoundEngine.PostEvent("TriggerSwipe", this.gameObject);
        AkSoundEngine.SetState("PlaylistStatesGroup", this._playlist_states[this._index_of_playlist]);
        this._bgm_title.text = this._playlist_titles[this._index_of_playlist];
    }
};

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class HedgeAction : MonoBehaviour
{
    public const string KOREO_EVENT_TRACK_MEASURE_CURVE = "TRACK_MEASURE_CURVE";
    
    RandomMovment _random_movement = new RandomMovment();

    public Animator _animator;
    public float    _animation_duration = 1.0f;
    
	void Start()
    {
        this.RandomColor();
        // this.RandomMove();
        //this.RandomPosition();
        this._animator = this.GetComponent<Animator>();        
        this.PrepareAnimation();
        this._animator.StartPlayback();        
        Koreographer.Instance.RegisterForEventsWithTime(KOREO_EVENT_TRACK_MEASURE_CURVE,
                                                        this.OnKoreoEventTrackMeasureCurve);
	}

    void OnDestroy()
    {
        if (Koreographer.Instance != null) {
            Koreographer.Instance.UnregisterForAllEvents(this);
        }
		if (this._animator) {
			this._animator.StopPlayback ();
		}
    }
    
	void FixedUpdate()
    {
        // this._random_movement.RandomMove(false);
	}

    void RandomColor()
    {     
        this.GetComponent<SpriteRenderer>().color = ColorPalette.RandomHedgeColor();
    }

    void RandomMove()
    {
        this._random_movement._transform = this.transform;
        this._random_movement._move_velocity = 3.0f;
        this._random_movement.RandomMove(true, null);        
    }

    void RandomPosition()
    {
        Camera camera = Camera.main;
        float view_port_r = camera.rect.xMin;
        float view_port_l = camera.rect.xMax;
        float view_port_t = camera.rect.yMin;
        float view_port_b = camera.rect.yMax;
        this.transform.position =
            camera.ViewportToWorldPoint(new Vector3(Random.Range(view_port_l, view_port_r),
                                                    Random.Range(view_port_b, view_port_t),
                                                    this.transform.position.z -
                                                    camera.transform.position.z));
    }

    void PrepareAnimation()
    {
        const float frame_rate = 32.0f;
        int frame_count = (int)(this._animation_duration * frame_rate);
        this._animator.Rebind();
        this._animator.StopPlayback();
        this._animator.recorderStartTime = 0;
        this._animator.StartRecording(frame_count);
        for (var i=0; i<frame_count-1; ++i) {
            this._animator.Update(1.0f / frame_rate);
        }
        this._animator.StopRecording();
    }

    void OnKoreoEventTrackMeasureCurve(KoreographyEvent koreoEvent,
                                       int sampleTime,
                                       int sampleDelta,
                                       DeltaSlice deltaSlice)
    {
        float value = koreoEvent.GetValueOfCurveAtTime(sampleTime);
        this._animator.playbackTime = Mathf.Lerp(this._animator.recorderStartTime,
                                                 this._animator.recorderStopTime,
                                                 value);
        this._animator.Update(0);
    }
};

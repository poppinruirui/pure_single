﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SonicBloom.Koreo;

public enum SceneStatus
{
    SceneStatusUnknown = 0,    
    SceneStatusWaiting,
    SceneStatusPlaying
};

public class SceneAction : MonoBehaviour
{
    public const string KOREO_EVENT_TRACK_EXPLOSION_BEAT = "TRACK_EXPLOSION_BEAT";
    
    Queue _ball_queue = new Queue();
    
    public GameObject _ball_prefab;    
    public int _num_of_ball_per_batch = 10;
    public int _num_of_ball_batch = 1;
    
    public GameObject[] _food_prefabs;
    public int _num_of_food_per_batch = 8;
    public int _num_of_food_batch = 5;
    
    Queue _food_queue = new Queue();

    float _last_refresh_food_time = 0.0f;
    float _refresh_food_interval = 5.0f;

    public GameObject _hedge_prefab;
    public int _num_of_hedge = 6;

    SceneStatus _scene_status = SceneStatus.SceneStatusUnknown;
        
    void Awake()
    {
        for (int i=0; i<this._num_of_ball_batch; ++i) {
            this.EnqueueBallInBatch();
        }
        for (int i=0; i<this._num_of_food_batch; ++i) {
            this.EnqueueFoodInBatch();
        }

        for (int i=0; i<this._num_of_hedge; ++i) {
            Instantiate(this._hedge_prefab);
        }        
    } 
        
	void Start()
    {
        this.SetSceneStatus(SceneStatus.SceneStatusWaiting);
        Koreographer.Instance.RegisterForEvents(KOREO_EVENT_TRACK_EXPLOSION_BEAT,
                                                this.OnKoreoEventTrackExplosionBeat);        
	}

    void OnDestroy()
    {
        if (Koreographer.Instance != null) {
            Koreographer.Instance.UnregisterForAllEvents(this);
        }
    }
	
	void Update()
    {		
	}

    void FixedUpdate()
    {
        this.RefreshFood();
    }

    void EnqueueBallInBatch()
    {
        for (int i=0; i<this._num_of_ball_per_batch; ++i) {
            GameObject ballObject = Instantiate(this._ball_prefab);
            this._ball_queue.Enqueue(ballObject);
        }
    }

    void DequeueBallInBatch()
    {
        for (int i=0; i<this._num_of_ball_per_batch; ++i) {
            if (this._ball_queue.Count > 0) {
                GameObject ballObject = (GameObject)this._ball_queue.Dequeue();
                GameObject.Destroy(ballObject);
            }
        }
    }    

    void DequeueFoodInBatch()
    {
        for (int i=0; i<this._num_of_food_per_batch; ++i) {
            if (this._food_queue.Count > 0) {
                GameObject foodObject = (GameObject)this._food_queue.Dequeue();
                GameObject.Destroy(foodObject);
            }
        }
    }
    
    void EnqueueFoodInBatch()
    {
        for (int i=0; i<this._num_of_food_per_batch; ++i) {
            int food__prefab_index = Random.Range(0, this._food_prefabs.Length - 1);
            GameObject foodObject = Instantiate(this._food_prefabs[food__prefab_index]);
            this._food_queue.Enqueue(foodObject);
        }        
    }
    
    void RefreshFood()
    {
        float now = Time.time;
        if (now - this._last_refresh_food_time > this._refresh_food_interval) {
            this.DequeueFoodInBatch();
            this.EnqueueFoodInBatch();    
            this._last_refresh_food_time = now;
        }
    }

    void OnLeaveStatusUnknown(SceneStatus old_status, SceneStatus new_status)
    {
    }

    void OnEnterStatusUnknown(SceneStatus old_status, SceneStatus new_status)
    {
    }

    void OnLeaveStatusWaiting(SceneStatus old_status, SceneStatus new_status)
    {
    }

    void OnEnterStatusWaiting(SceneStatus old_status, SceneStatus new_status)
    {
        GUIAction gui = this.GetComponent<GUIAction>();
        gui._product_title.SetActive(true);
        gui._bgm_title.gameObject.SetActive(true);
        gui._play_button.gameObject.SetActive(true);
        gui._next_button.gameObject.SetActive(true);
        gui._prev_button.gameObject.SetActive(true);
        gui._menu_button.gameObject.SetActive(false);
        gui._home_button.gameObject.SetActive(false);
        gui._shoot_button.gameObject.SetActive(false);
        gui._split_button.gameObject.SetActive(false);
        gui._ctrl_mode_dropdown.gameObject.SetActive(false);
        gui._dir_indicator_type_dropdown.gameObject.SetActive(false);
  //      CtrlMode.SetCtrlMode(CtrlMode.CTRL_MODE_NONE);
        CtrlMode.SetDirIndicatorType(CtrlMode.DIR_INDICATOR_NONE);
  //      gui.UpdateCtrlModeDropDown(CtrlMode.CTRL_MODE_NONE);
        gui.UpdateDirIndicatorDropDown(CtrlMode.DIR_INDICATOR_NONE);
    }

    void OnLeaveStatusPlaying(SceneStatus old_status, SceneStatus new_status)
    {
    }

    void OnEnterStatusPlaying(SceneStatus old_status, SceneStatus new_status)
    {
        GUIAction gui = this.GetComponent<GUIAction>();        
        gui._product_title.SetActive(false);
        gui._bgm_title.gameObject.SetActive(false);
        gui._play_button.gameObject.SetActive(false);
        gui._next_button.gameObject.SetActive(false);
        gui._prev_button.gameObject.SetActive(false);
        gui._menu_button.gameObject.SetActive(true);
        gui._home_button.gameObject.SetActive(true);
        gui._shoot_button.gameObject.SetActive(true);
        gui._split_button.gameObject.SetActive(true);
        gui._ctrl_mode_dropdown.gameObject.SetActive(true);
        gui._dir_indicator_type_dropdown.gameObject.SetActive(true);
    }
    
    void OnLeaveStatus(SceneStatus old_status, SceneStatus new_status)
    {
        switch (old_status) {
            case SceneStatus.SceneStatusUnknown:
                this.OnLeaveStatusUnknown(old_status, new_status);
                break;
            case SceneStatus.SceneStatusWaiting:
                this.OnLeaveStatusWaiting(old_status, new_status);                
                break;
            case SceneStatus.SceneStatusPlaying:
                this.OnLeaveStatusPlaying(old_status, new_status);
                break;
            default:
                Debug.Log("invalid old status: " + old_status);
                break;
        }
    }

    void OnEnterStatus(SceneStatus old_status, SceneStatus new_status)
    {
        switch (new_status) {
            case SceneStatus.SceneStatusUnknown:
                this.OnEnterStatusUnknown(old_status, new_status);                
                break;
            case SceneStatus.SceneStatusWaiting:
                this.OnEnterStatusWaiting(old_status, new_status);                                
                break;
            case SceneStatus.SceneStatusPlaying:
                this.OnEnterStatusPlaying(old_status, new_status);                
                break;
            default:
                Debug.Log("invalid new status: " + new_status);
                break;
        }
    }    

    public SceneStatus GetSceneStatus()
    {
        return this._scene_status;
    }
    
    public void SetSceneStatus(SceneStatus status)
    {
        SceneStatus old_status = this._scene_status;
        SceneStatus new_status = status;
        this.OnLeaveStatus(old_status, new_status);
        this._scene_status = status;
        this.OnEnterStatus(old_status, new_status);
    }

    void OnKoreoEventTrackExplosionBeat(KoreographyEvent koreoEvent)
    {
        int value = koreoEvent.GetIntValue();
        if (value < 0) {
            this.DequeueBallInBatch();
            this.EnqueueBallInBatch();
            Debug.Log("OnKoreoEventTrackExplosionBeat");            
        }
    }        
}

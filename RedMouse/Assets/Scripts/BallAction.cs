﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class BallAction : MonoBehaviour
{
    public const string KOREO_EVENT_TRACK_EXPLOSION_BEAT = "TRACK_EXPLOSION_BEAT";
        
    public float _total_scale_min = 0.1f;
    public float _total_scale_max = 4.0f;
    public float _total_scale_ratio = 0.0f;
    public float _total_scale_inpoisonable_ratio = 0.3f;
    
    public float _poison_scale_min = 0.0f;
    public float _poison_scale_max = 1.0f;
    public float _poison_scale_inc = 0.0f;    
    public float _poison_scale_inc_delta = 0.1f;

	public Vector2 _direction = new Vector2( -0.5f, -0.5f );
	public float _ball_realtime_velocity = 0.0f;

    int _color_index = -1;

    RandomMovment _random_movement = new RandomMovment();

    public float _velocity = 0.2f;    
    const float _dir_space = 0.7f;
    
    public GameObject _dir_indicator;
    public LineRenderer _dir_line;

	Ball _ball = null; 

	void Awake()
	{
		_ball = this.gameObject.GetComponent<Ball> ();
	}

	void Start()
    {
     //   this.RandomColor();
     //   this.RandomScale(true);

        this._total_scale_ratio = (this.transform.localScale.x / this._total_scale_max);
   //     this._poison_scale_inc_delta /= this._total_scale_ratio;
        
    //    this._random_movement._transform = this.transform;        
    //    this._random_movement._move_velocity /= this._total_scale_ratio;
    //    this.RandomPoisonFill(true);
   //     this._random_movement.RandomMove(true, null);
   //     Koreographer.Instance.RegisterForEvents(KOREO_EVENT_TRACK_EXPLOSION_BEAT,
   //                                             this.OnKoreoEventTrackExplosionBeat);
	}

    void OnDestroy()
    {
        if (Koreographer.Instance != null) {
            Koreographer.Instance.UnregisterForAllEvents(this);
        }
    }    
	
	void Update()
    {
	}

	void FixedUpdate()
    {
        if (false/*CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_NONE*/) {
          //  this._random_movement.RandomMove(false, null);
        }
        if (CtrlMode.GetDirIndicatorType() == CtrlMode.DIR_INDICATOR_NONE) {
            this._dir_line.enabled = false;
            this._dir_indicator.GetComponent<Renderer>().enabled = false;            
        }
        //this.RandomScale(false);
        //this.RandomPoisonFill(false);
	}

    void UpdateDirIndicator(Vector3 cursor_position, Vector2 direction)
    {
       if (CtrlMode.GetDirIndicatorType() == CtrlMode.DIR_INDICATOR_ARROW) {
            this._dir_line.enabled = false;
            this._dir_indicator.GetComponent<Renderer>().enabled = true;
            this._dir_indicator.transform.localPosition =
                new Vector3(direction.x * BallAction._dir_space,
                            direction.y * BallAction._dir_space,
                            this._dir_indicator.transform.localPosition.z);
            this._dir_indicator.transform.rotation = Quaternion.FromToRotation(Vector2.right, direction);
        }
        else if (CtrlMode.GetDirIndicatorType() == CtrlMode.DIR_INDICATOR_LINE) {
            this._dir_line.enabled = true;
//            this._dir_indicator.GetComponent<Renderer>().enabled = false;
            this._dir_line.SetPosition(0, new Vector3(cursor_position.x,
                                                      cursor_position.y,
                                                      this._dir_line.transform.position.z));
            this._dir_line.SetPosition(1, new Vector3(/*this.transform.position.x,
                                                      this.transform.position.y,*/ _ball.outter.transform.position.x, _ball.outter.transform.position.y,
                                                      this._dir_line.transform.position.z));
			this._dir_indicator.GetComponent<Renderer>().enabled = false;
        }
        else {
            this._dir_line.enabled = false;
            this._dir_indicator.GetComponent<Renderer>().enabled = false;
        }
    }

    public void UpdatePosition(Vector3 cursor_position, float ball_velocity)
    {
		// 被吐出来的新球正在冲刺状态的时候，不受操控，不跟随队伍移动
		if (_ball && _ball.IsNewBallRunning ()) {
			return;
		}

		if (this._total_scale_ratio == 0.0f) {
			return;
		}

		_direction = cursor_position - this.transform.position;
		_direction.Normalize();

		this._total_scale_ratio = (this.transform.localScale.x / this._total_scale_max);

        _ball_realtime_velocity = ball_velocity / this._total_scale_ratio;
		Vector3 vecTemp = new Vector3 (_direction.x, _direction.y, 0);
		vecTemp = vecTemp * _ball_realtime_velocity * Time.fixedDeltaTime;

		this.transform.position += vecTemp;
            
		this.UpdateDirIndicator(cursor_position, _direction);
    }
    
    void RandomColor()
    {
		return;

        this._color_index = 0;
        GameObject outer_ring = this.transform.Find("OuterRing").gameObject;
        outer_ring.GetComponent<SpriteRenderer>().color =
            ColorPalette.RandomOuterRingColor(ref this._color_index);
        GameObject inner_fill = this.transform.Find("OuterRing/InnerFill").gameObject;
        inner_fill.GetComponent<SpriteRenderer>().color =
            ColorPalette.RandomInnerFillColor(this._color_index);
        GameObject poison_fill = this.transform.Find("OuterRing/InnerFill/PoisonFill").gameObject;
        poison_fill.GetComponent<SpriteRenderer>().color =
            ColorPalette.RandomPoisonFillColor(this._color_index);
        Gradient gradient = new Gradient();
        gradient.SetKeys(new GradientColorKey[] {
                new GradientColorKey(ColorPalette.RandomPoisonFillColor(this._color_index), 0.0f),
                new GradientColorKey(Color.white, 1.0f)
            },
            new GradientAlphaKey[] {
                new GradientAlphaKey(0.0f, 0.0f),
                new GradientAlphaKey(1.0f, 1.0f)
            });
        this._dir_line.colorGradient = gradient;
    }

    void RandomPoisonFill(bool immediately)
    {
		return;

        GameObject poison_fill = this.transform.Find("OuterRing/InnerFill/PoisonFill").gameObject;        
        float poison_scale = 0.0f;
        float poison_scale_z = poison_fill.GetComponent<Transform>().localScale.z;
        if (immediately) {
            poison_scale = Random.Range(this._poison_scale_min,
                                        this._poison_scale_max);
        }
        else {            
            poison_scale = Mathf.Lerp(this._poison_scale_min,
                                      this._poison_scale_max,
                                      this._poison_scale_inc);
            this._poison_scale_inc += this._poison_scale_inc_delta * Time.fixedDeltaTime;
            if (this._poison_scale_inc >= this._poison_scale_max) {
                float scale_tmp = this._poison_scale_min;
                this._poison_scale_min = this._poison_scale_max;
                this._poison_scale_max = scale_tmp;
                this._poison_scale_inc = 0.0f;
            }
        }
        if (this._total_scale_ratio < this._total_scale_inpoisonable_ratio) {
            poison_scale = 0.0f;
        }
        poison_fill.GetComponent<Transform>().localScale =
            new Vector3(poison_scale, poison_scale, poison_scale_z);
    }
    
    void RandomScale(bool immediately)
    {
        if (immediately) {
            GameObject outer_ring = this.gameObject;
            float total_scale = Random.Range(this._total_scale_min,
                                             this._total_scale_max);
            float total_scale_z = outer_ring.GetComponent<Transform>().localScale.z;
            outer_ring.GetComponent<Transform>().localScale = new Vector3(total_scale, total_scale, total_scale_z);
        }
    }

    void OnKoreoEventTrackExplosionBeat(KoreographyEvent koreoEvent)
    {
        int value = koreoEvent.GetIntValue();
        GameObject inner_fill = this.transform.Find("OuterRing/InnerFill").gameObject;
        GameObject poison_fill = this.transform.Find("OuterRing/InnerFill/PoisonFill").gameObject;
        if (value > 0) {
            inner_fill.GetComponent<SpriteRenderer>().color =
                ColorPalette.RandomInnerFillColor(this._color_index);
            poison_fill.GetComponent<SpriteRenderer>().color =
                ColorPalette.RandomPoisonFillColor(this._color_index);            
        }
        else if (value == 0) {
            inner_fill.GetComponent<SpriteRenderer>().color =
                ColorPalette.RandomPoisonFillColor(this._color_index);
            poison_fill.GetComponent<SpriteRenderer>().color =
                ColorPalette.RandomInnerFillColor(this._color_index);
        }
    }    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitBallTarget : MonoBehaviour {

	Ball m_ballCaster = null;
	SpriteRenderer m_sr = null;
	Vector3 m_vecTemp = new Vector3();

	// Use this for initialization
	void Start () {
	
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	bool CheckIfCasterDestroyed()
	{
		return m_ballCaster == null;
	}

	public void SetCaster( Ball ballCaster )
	{
		m_ballCaster = ballCaster;
	}


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : Photon.PunBehaviour , IPunObservable 
{
    GameObject m_goLine;

	GameObject m_goShell; 
	public GameObject shell
	{
		get { return m_goShell; }
		set { m_goShell = value; }
	}

	GameObject m_goInnerBall; 
	public GameObject inner
	{
		get { return m_goInnerBall; }
		set { m_goInnerBall = value; }
	}

	CircleCollider2D m_Collider;
	public CircleCollider2D _collider
	{
		get { return m_Collider; }
		set { m_Collider = value;  }
	}

	GameObject m_goTrigger; // 最主要的触发器，包括“临时的翼展”机制也是靠它实现的
	public GameObject _trigger
	{
		get { return  m_goTrigger; }
		set { m_goTrigger = value;  }
	}

	SpriteRenderer m_srTrigger;
	public SpriteRenderer _triggersr
	{
		get { return m_srTrigger; }
		set { m_srTrigger = value; }
	}

	CircleCollider2D m_colliderTrigger;
	public CircleCollider2D _colliderTrigger
	{
		get { return m_colliderTrigger; }
		set { m_colliderTrigger = value; }
	}

	GameObject m_goThorn;
	public GameObject thorn
	{
		get { return m_goThorn;  }
		set { m_goThorn = value; }
	}

	SpriteRenderer m_srInnerFill;
	public SpriteRenderer _srInnerFill
	{
		set { m_srInnerFill = value; }
		get { return m_srInnerFill;  }
	}
	protected GameObject m_goOutterBall;
	public GameObject outter
	{
		get { return m_goOutterBall; }
		set { m_goOutterBall = value; }
	}

    float m_fKX = -1.0f, m_fKY = 0.0f;
	public float _kx
	{
		set {  
			if (_ba == null) {
				return;
			}
			_ba._direction.x = value; 
		}
		get { 
			if (_ba == null) {
				return 0.0f;
			}
			return _ba._direction.x; 

		}
	}
	public float _ky
	{
		set {  
			if (_ba == null) {
				return;
			}
			_ba._direction.y = value; 
		}
		get { 
			if (_ba == null) {
				return 0.0f;
			}
			return _ba._direction.y; 

		}
	}
    float m_fNewBallRunKX, m_fNewBallRunKY;


	public ObsceneMaskShader _obsceneCircle;

    float m_fTargetSize = 1.0f;


	float m_fSpeed = 0.0f;
	public float speed
	{
		get { 
				if (m_bNewBallRun) 
				{
					return m_fNewBallRunSpeed / Time.fixedDeltaTime;
				} 
				else
				{
					return m_fSpeed;
				}
			}
		set { 
				SetSpeed( value );
			}
	}

	public Player _player = null; // 本Ball所属的Player
	GameObject m_goPlayer = null; // 这个 ball所属的player
	Player m_MainPlayer = null; // 当前客户端的mainpalyer

	bool m_bIsInited = false;
	public bool isInited
	{
		get{ return m_bIsInited; }
		set {m_bIsInited = value; }
	}
		
	public int ownerId
	{
		get { return photonView.ownerId;  }
	}

	PhotonTransformView m_PhoTransView;

	bool m_bIsThorn = false; // 是不是“刺”
	public bool isThorn
	{
		set { m_bIsThorn = value; }
		get { return m_bIsThorn; }
	}

	eBallType m_eBallType = eBallType.ball_type_ball;
	public eBallType _balltype
	{
		set {  m_eBallType = value; }
		get {  return m_eBallType; }
	}

	public enum eBallType
	{
		ball_type_ball,          // 常规的球
		ball_type_thorn,         // 刺
		ball_type_bean,          // 豆子
	};

	public BallAction _ba; // 小熊那边做的BallAction模块

	// Use this for initialization
	// !!!! 太坑爹了，Start()中的Init()并非在对象创建的那一帧执行，而是在该Object创建之后的下一帧才执行
	void Start () 
    {

		// poppin test
		/*
		object[] info = photonView.instantiationDataField;
		if (info != null) {
			Debug.Log ("我日你个龟：" + info [0]);
		} else {
			Debug.Log ( "肿么回事？？" );
		}
		*/
			
	}

	void Awake()
	{
		Init ();
	}

	public virtual void Init()
	{
		if (isInited) {
			return;
		}

		isInited = true;

		GameObject go;

		// 这儿有隐患(今后一律不能有isLocal、isMainPlayer之类的机制)，抽空改一下！！！！！
		if (photonView.isMine) {
			m_goPlayer = GameObject.Find ("Balls/MainPlayer");
		} else {
			m_goPlayer = GameObject.Find ("Balls/player_" + photonView.ownerId);
		}

		if (m_goPlayer == null) {
			isInited = false;
			return;
		}
		_player = m_goPlayer.GetComponent<Player> ();

		this.gameObject.transform.parent = m_goPlayer.transform;

		go = GameObject.Find ( "Balls/MainPlayer" );
		m_MainPlayer = go.GetComponent<Player> ();
		if (m_MainPlayer == null) {
			isInited = false;
			return;
		}

		Transform transOuuter = this.gameObject.transform.Find ("InnerFill"); 
		if (transOuuter) {
			m_goOutterBall = transOuuter.gameObject;
		}

		if ( m_goOutterBall == null)
		{
			isInited = false;
			Debug.LogError ( "木得外环怎么行" );
			return;
		}

		_srInnerFill = outter.GetComponent<SpriteRenderer> ();
		_srInnerFill.color = _player._color_inner;

		m_Collider = m_goOutterBall.GetComponent<CircleCollider2D> ();

		Transform transShell = outter.transform.Find ("OuterRing"); 
		if (transShell) {
			m_goShell = transShell.gameObject;
			SpriteRenderer sr = m_goShell.GetComponent<SpriteRenderer> ();
			sr.color = _player._color_ring;
		}

		Transform transThorn = this.gameObject.transform.Find ("PoisonFill"); 
		if ( transThorn )
		{
			m_goThorn = transThorn.gameObject;
			SpriteRenderer sr = m_goThorn.GetComponent<SpriteRenderer> ();
			sr.color = _player._color_poison;
			transThorn.localScale = Vector3.zero;
		}

		Transform transMainTrigger = this.gameObject.transform.Find ("MainTrigger"); 
		if ( transMainTrigger )
		{
			m_goTrigger = transMainTrigger.gameObject;
			_triggersr = m_goTrigger.GetComponent<SpriteRenderer> ();
			_colliderTrigger = m_goTrigger.GetComponent<CircleCollider2D> ();
			//sr.color = ColorPalette.GetBallMainColorByIdx(photonView.ownerId);
			Color colorTemp = _player._color_inner;
			colorTemp.a = 0.0f;
			_triggersr.color = colorTemp;
			transMainTrigger.localScale = new Vector3 ( 1.0f, 1.0f, 1.0f );

		}



		m_PhoTransView = GetComponent<PhotonTransformView> ();

		if (m_PhoTransView == null) {
			isInited = false;
			return;
		}

		outter.layer = photonView.ownerId + 7;

		if (m_goOutterBall) {
			SpriteRenderer sr = m_goOutterBall.GetComponent<SpriteRenderer> ();

			SetId (photonView.ownerId);
			/*
			if (photonView.ownerId == 1) {
				sr.color = Color.red;

			} else if (photonView.ownerId == 2) {
				sr.color = Color.green;

			} else if (photonView.ownerId == 3) {
				sr.color = Color.yellow;

			} else {
				sr.color = Color.blue;
			}
			*/
			//sr.color = ColorPalette.GetBallMainColorByIdx(photonView.ownerId);
		
		}



		ReCalcMySpeed ();

		GameObject goObsceneMask = this.gameObject.transform.FindChild ( "ObsceneCircle" ).gameObject;
		_obsceneCircle = goObsceneMask.GetComponent<ObsceneMaskShader> ();


		_ba = this.gameObject.GetComponent<BallAction> ();
	}

	void ReCalcMySpeed()
	{
		speed = Main.s_Instance.CalculateBallSpeed ( this );
	}

	void SwitchSyncMode()
	{
		if (IsPking ()) {
			SwitchSyncPositionMode (PhotonTransformViewPositionModel.InterpolateOptions.Disabled);
		} else {
			SwitchSyncPositionMode (PhotonTransformViewPositionModel.InterpolateOptions.Lerp);
		}
	}
	
	// Update is called once per frame
	void UpdateFollowOutter()
	{
		if (thorn && outter) {
			Vector3 vecTemp = outter.transform.localPosition;
			vecTemp.z = thorn.transform.localPosition.z;
			thorn.transform.localPosition = vecTemp;
		}

		if (_trigger && outter) {
			Vector3 vecTemp = outter.transform.localPosition;
			vecTemp.z = thorn.transform.localPosition.z;
			_trigger.transform.localPosition = vecTemp;
		}

		if (_obsceneCircle && outter) {
			Vector3 vecTemp = outter.transform.localPosition;
			vecTemp.z = _obsceneCircle.transform.localPosition.z;
			_obsceneCircle.transform.localPosition = vecTemp;
		}
	}

	// Update is called once per frame
	public virtual void Update () 
    {
		if (!isInited) {
			Init ();
		}

		UpdateFollowOutter ();

		PreUnfoldLoop ();
		UnfoldLoop ();


		DigestSize ();

		NewBallRun();
		ThornAttenuate ();

		if ( CheckIfExplodeNow () )
		{
			// Explode();
			if ( !m_bIsPreExploding )
			{
				BeginPreExplode(); // 预爆炸
			}
		}
		PreExplode ();

		ShellShrink ();
	}

    void FixedUpdate()
    {
		//InnerBallGrow ();

    }

    float m_fNewBallRunSpeed = 0.0f;
    public bool m_bNewBallRun = false;
	Vector3 m_vecRushStartPos = new Vector3();
	public void BeginNewBallRun( float fSpeed )
    {
		photonView.RPC ( "RPC_BeginNewBallRun", PhotonTargets.All, this.gameObject.transform.position, _kx, _ky, fSpeed );
    }

	[PunRPC]
	public void RPC_BeginNewBallRun( Vector3 vecStartPos, float KX, float KY, float fSpeed )
	{
		m_bNewBallRun = true;
		m_fNewBallRunKX = KX;
		m_fNewBallRunKY = KY;
		m_fNewBallRunSpeed = fSpeed;
		this.gameObject.transform.localPosition = vecStartPos;

		//photonView.RPC ( "ZouNi", PhotonTargets.All, m_vecRushStartPos, m_fKX, m_fKY  );
	}

    public void EndNewBallRun()
    {
        //m_bNewBallRun = false;

		photonView.RPC ( "RPC_EndNewBallRun", PhotonTargets.All );
    }

	[PunRPC]
	public void RPC_EndNewBallRun()
	{
		m_bNewBallRun = false;
		m_nCasterId = 0;
		if (m_ballCaster) {
			m_ballCaster.DestroySpitBallTargetCircle ();
		}
	}

	public bool IsNewBallRunning()
	{
		return m_bNewBallRun;
	}
	
    public void NewBallRun()
    {
        if (!m_bNewBallRun)
        {
            return;
        }

        if (m_fNewBallRunSpeed <= 0.0f)
        {
            EndNewBallRun();
			return;
        }

		Vector3 vecTemp = this.gameObject.transform.position;
		vecTemp.x += m_fNewBallRunSpeed * m_fNewBallRunKX * Time.deltaTime;
		vecTemp.y += m_fNewBallRunSpeed * m_fNewBallRunKY * Time.deltaTime;
        this.gameObject.transform.position = vecTemp;
		m_fNewBallRunSpeed += Main.s_Instance.m_fNewBallRunSpeedAccelerate * Time.deltaTime;
    }
    /*
    public void BeginUpdateSize(float fMyCurSize)
    {
        m_fTargetSize = fMyCurSize;
        m_bUpdatingSize = true;
        //SetSize(fMyCurSize);
    }
	
    public void EndUpdateSize()
    {
        m_bUpdatingSize = false;
    }
*/



	public void SetSpeed( float fSpeed )
	{
		photonView.RPC ("RPC_SetSpeed", PhotonTargets.All, fSpeed);
	}

	[PunRPC]
	public void RPC_SetSpeed( float fSpeed )
	{
		m_fSpeed = fSpeed;
	}
		
    public void SetSize( float fSize )
    {
		//outter.transform.localScale = new Vector3(fSize, fSize, 1.0f); // 这是2D游戏，不要随便去改z坐标的值，否则容易出

		photonView.RPC ("RPC_SetSize", PhotonTargets.All, fSize);


    }

	[PunRPC]
	public void RPC_SetSize( float fSize )
	{
		Vector3 vecTemp = outter.transform.position; 
	
		
		this.gameObject.transform.localScale = new Vector3(fSize, fSize, 1.0f); // 这是2D游戏，不要随便去改z坐标的值，否则容易出问题

		//vecTemp = outter.transform.position - vecTemp;
		//Vector3 vecTemp1 = outter.transform.localPosition;
		//vecTemp1 -= vecTemp;
		outter.transform.position = vecTemp;

		ReCalcMySpeed();
		LocalSetCurThornTotalSize (GetCurThornTotalSize ());

		UpdateFollowOutter ();
		//Vector3 vecTemp = this.gameObject.transform.position;
		//vecTemp.z = -fSize;
		//this.gameObject.transform.position = vecTemp;



	}
		
	public void SetInnerBallSize( float fInnnerBallSize )
	{
		return;

		/*
		if ( inner == null )
		{
			return;
		}
		inner.transform.localScale = new Vector3(fInnnerBallSize, fInnnerBallSize, 1.0f);	
		*/
		photonView.RPC ( "RPC_SetInnerBallSize", PhotonTargets.All, fInnnerBallSize );
	}

	[PunRPC]
	public void RPC_SetInnerBallSize( float fInnnerBallSize )
	{
		if ( inner == null )
		{
			return;
		}
		inner.transform.localScale = new Vector3(fInnnerBallSize, fInnnerBallSize, 1.0f);		
	}

	public float GetInnerBallSize()
	{
		if (inner == null) {
			return 1.0f;
		}
		return inner.transform.localScale.x;
	}

	/// <summary>
	/// / 废弃
	/// </summary>
	bool m_bIsInnerBallGrowing = false;
	float m_fGrowSpeed = 0.002f;

	bool m_bIsShellShrinking = false;


	void SetIfInnerBallGrowing(bool bIfInnerBallGrowing)
	{
		//m_bIsInnerBallGrowing = bIfInnerBallGrowing;
		photonView.RPC ( "RPC_SetIfInnerBallGrowing", PhotonTargets.All, bIfInnerBallGrowing );
	}

	[PunRPC]
	void RPC_SetIfInnerBallGrowing(bool bIfInnerBallGrowing)
	{
		m_bIsInnerBallGrowing = bIfInnerBallGrowing;
	}

	// 废弃
	public void InnerBallGrow ()
	{
		if (!m_bIsInnerBallGrowing) 
		{
			return;
		}

		Vector3 vecCurScale = m_goInnerBall.transform.localScale;
		vecCurScale.x += m_fGrowSpeed;
		vecCurScale.y += m_fGrowSpeed;
		vecCurScale.z += m_fGrowSpeed;
		m_goInnerBall.transform.localScale = vecCurScale;

		if (m_goInnerBall.transform.localScale.x >= 1.0f) 
		{
			vecCurScale.x = vecCurScale.y = vecCurScale.z = 1.0f;
			m_goInnerBall.transform.localScale = vecCurScale;
			EndInnerBallGrow();
		}

	}

	float m_fShellShrinkTime = 5.0f; // 坍缩时间5秒
	// 外壳坍缩
	void ShellShrink()
	{
		if (m_bIsPreExploding) {
			return;
		}

		if (!m_bIsShellShrinking) 
		{
			return;
		}


		Vector3 vecCurScale = new Vector3 ();
		vecCurScale = shell.transform.localScale;
		vecCurScale.x += Main.s_Instance.m_fShellShrinkSpeed;
		vecCurScale.y += Main.s_Instance.m_fShellShrinkSpeed;
		vecCurScale.z += Main.s_Instance.m_fShellShrinkSpeed;
		shell.transform.localScale = vecCurScale;

		if (shell.transform.localScale.x <= 1.0f) 
		{
			vecCurScale.x = vecCurScale.y = vecCurScale.z = 0.0f;
			shell.transform.localScale = vecCurScale;
			EndShellShrink ();
		}
	}

	// 外壳是否正在坍缩阶段
	public bool IsShellShrink()
	{
		return m_bIsShellShrinking;
	}
		
	// 外壳开始萎缩
	public void BeginShellShrink()
	{
		photonView.RPC ( "RPC_BeginShellShrink", PhotonTargets.All );

	}

	[PunRPC]
	public void RPC_BeginShellShrink()
	{
		shell.transform.localScale = new Vector3( 1.1f, 1.1f, 1.0f );
		m_bIsShellShrinking = true;
		m_fShellShrinkTime = 5.0f;
		_collider.isTrigger = false;
	}

	// 外壳停止收缩
	public void EndShellShrink()
	{
		photonView.RPC ( "RPC_EndShellShrink", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_EndShellShrink()
	{
		m_bIsShellShrinking = false;
		_collider.isTrigger = true;
	}

	/// <summary>
	/// 中环机制废弃
	/// </summary>
	public void BeginInnerBallGrow()
	{
		SetIfInnerBallGrowing ( true );
	}

	public void EndInnerBallGrow()
	{
		SetIfInnerBallGrowing ( false );
	}

    public float GetSize()
    {
		if (isThorn) {
			return 1.0f;
		}

		return this.gameObject.transform.localScale.x;
    }

	public float GetCurThornTotalSize()
	{
		if (thorn == null) {
			return 0.0f;
		}
		return m_fThornTotalSize;
	}
		
	protected float m_fThornTotalSize = 0.0f;

	// 球身上带的刺会自动衰减
	void ThornAttenuate()
	{

		float fCurSize = GetCurThornTotalSize ();
		if (fCurSize == 0.0f) {
			return;
		}
		float fSize = fCurSize - Main.s_Instance.m_fThornAtteuateSpeed * Time.deltaTime;
		if (fSize < 0.0f) {
			fSize = 0.0f;
		}
		LocalSetCurThornTotalSize ( fSize );
	}

	void LocalSetCurThornTotalSize( float  fNewSize)
	{
		if (thorn == null) {
			return;
		}
		float fBallSize = GetSize ();
		m_fThornTotalSize = fNewSize; // 绝对大小
		Vector3 vecTemp = new Vector3();
		vecTemp.x = fNewSize / fBallSize;
		vecTemp.y = fNewSize / fBallSize;
		vecTemp.z = 1.0f;
		thorn.transform.localScale = vecTemp;  // 实际设置的时候要设相对大小，因为Ball整体设置Scale的时候对刺的尺寸也是有加成的		
	}

	public void SetCurThornTotalSize( float fNewSize )
	{
		photonView.RPC ("RPC_SetCurThornTotalSize", PhotonTargets.All, fNewSize);
	}

	[PunRPC]
	public void RPC_SetCurThornTotalSize( float fNewSize )
	{
		LocalSetCurThornTotalSize (fNewSize);
	}

	public void IncreaseCurThornTotalSize( float fNewSize )
	{
		if (m_fThornTotalSize >= fNewSize) {
			return;
		}

		photonView.RPC ("RPC_IncreaseCurThornTotalSize", PhotonTargets.All, fNewSize);
	}

	[PunRPC]
	public void RPC_IncreaseCurThornTotalSize( float fNewSize )
	{
		if (m_fThornTotalSize >= fNewSize) {
			return;
		}

		LocalSetCurThornTotalSize (fNewSize);
	}

	public void DecreaseCurThornTotalSize( float fNewSize )
	{
		if (m_fThornTotalSize <= fNewSize) {
			return;
		}

		photonView.RPC ("RPC_DecreaseCurThornTotalSize", PhotonTargets.All, fNewSize);
	}

	[PunRPC]
	public void RPC_DecreaseCurThornTotalSize( float fNewSize )
	{
		if (m_fThornTotalSize <= fNewSize) {
			return;
		}

		LocalSetCurThornTotalSize (fNewSize);
	}
		
    public void SetLine(GameObject goLine)
    {
        m_goLine = goLine;
		m_goLine.SetActive( false );
    }

    public GameObject GetLine()
    {
        return m_goLine;
    }

    public void SetMoveDir( float fKX, float fKY )
    {
        //m_fKX = fKX;
        //m_fKY = fKY;

		if (_ba == null) {
			return;
		}

		_ba._direction.x = fKX;
		_ba._direction.y = fKY;
    }

    public void GetMoveDir(ref float fKX, ref float fKY)
    {
        //fKX = m_fKX;
        //fKY = m_fKY;
		if (_ba == null) {
			fKX = 0.0f;
			fKY = 0.0f;
			return;
		}

		fKX = _ba._direction.x;
		fKY = _ba._direction.y;
    }
	
	Ball m_ballOpponent = null;
	public void SetPkOppoent( Ball ball )
	{
		m_ballOpponent = ball;
	}

	List<Ball> m_lstPkOpponent = new List<Ball>();
	public void AddPkOpponent( Ball ball )
	{
		m_lstPkOpponent.Add ( ball );
	}

	public void RemovePkOpponent( Ball ball )
	{
		m_lstPkOpponent.Remove (ball);
	}

	public void ProcessPk( Ball ballOpponent )
	{
		if ( _player && _player._isRebornProtected ) { // 处于重生保护期不参加任何PK 
			return;
		}

		if (ballOpponent._player && ballOpponent._player._isRebornProtected) {
			return;
		}

		if (_dead || ballOpponent._dead) { // 都死翘翘了还PK个毛线。 为什么要加这一条容错：因为unity的底层机制决定的，想要销毁的东东未必会在执行销毁的那一帧真正执行，可能是在下一帧猜执行
			return;
		}

		if (_collider.bounds.size.x == ballOpponent._collider.bounds.size.x) { // 平局
			return;
			}
		else if ( _collider.bounds.size.x > ballOpponent._collider.bounds.size.x )
			{
			this.PreEat ( ballOpponent );
			}
			else
			{
			ballOpponent.PreEat ( this );
			}

	}

	int g_nShit = 1;
	void PreEat( Ball ballOpponent )
	{
		if (this._balltype == eBallType.ball_type_bean || this._balltype == eBallType.ball_type_thorn) {
			return;
		}

		if (!CheckIfTotallyCover ( this._colliderTrigger, ballOpponent._colliderTrigger )) {
			return;
		}


		this.Eat(ballOpponent);

	}

	bool CheckIfTotallyCover( CircleCollider2D circle1, CircleCollider2D circle2  )
	{
		float r1 = circle1.bounds.size.x / 2.0f;
		float r2 = circle2.bounds.size.x / 2.0f;
	
		float deltaX = circle1.bounds.center.x - circle2.bounds.center.x;
		float deltaY = circle1.bounds.center.y - circle2.bounds.center.y;
		float dis = Mathf.Sqrt ( deltaX * deltaX + deltaY * deltaY );
		//Debug.Log ( dis + "," +  Mathf.Abs (r1 - r2) );
		//Debug.Log( circle1.bounds.center.x + " , " + circle1.bounds.center.y + " , " + circle2.bounds.center.x + " , " + circle2.bounds.center.y + " , " +  dis + " , " + Mathf.Abs (r1 - r2)  );
		if (dis <= Mathf.Abs (r1 - r2)) {
			return true;
		}

		return false;
	}

	public bool CheckIfCanUnfold()
	{
		if (m_bIsPreUnfold || m_bIsUnfold) {
			return false;
		}

		return true;
	}

	float m_fPreUnfoldTimeLeft = 0.0f;
	bool m_bIsPreUnfold = false;
	float m_fUnfoldTimeLeft = 0.0f;
	bool m_bIsUnfold = false;
	public void BeginPreUnfold()
	{
		ObscenelyPreUnfold ();
		m_fPreUnfoldTimeLeft = Main.s_Instance.m_fMainTriggerPreUnfoldTime;
		m_bIsPreUnfold = true;

	}

	// 猥琐地展开
	public void ObscenelyPreUnfold()
	{
		photonView.RPC ( "RPC_ObscenelyPreUnfold", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_ObscenelyPreUnfold()
	{
		_obsceneCircle.Begin ();
		//_trigger.transform.localScale = new Vector3( Main.s_Instance.m_fMainTriggerBaseScale, Main.s_Instance.m_fMainTriggerBaseScale, 1.0f );
		//Color colorTemp = _triggersr.color;
		//colorTemp.a = 0.4f;
		//_triggersr.color = colorTemp;
	}

	// 猥琐地收拢
	public void Obscenelyfold()
	{
		photonView.RPC ( "RPC_Obscenelyfold", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_Obscenelyfold()
	{
		_trigger.transform.localScale = new Vector3( 1.0f, 1.0f, 1.0f );
		Color colorTemp = _triggersr.color;
		colorTemp.a = 0.0f;
		_triggersr.color = colorTemp;
	}

	public void PreUnfoldLoop()
	{
		if (!m_bIsPreUnfold) {
			return;
		}
		/*
		m_fPreUnfoldTimeLeft -= Time.deltaTime;
		if (m_fPreUnfoldTimeLeft <= 0.0f) {
			BeginUnfold ();
		}

		*/
		if (_obsceneCircle.IsEnd ()) {
			BeginUnfold ();
		}
	}

	public void BeginUnfold()
	{
		Obscenelyfold ();
		m_bIsPreUnfold = false;
		m_bIsUnfold = true;
	
		//outter.transform.localScale = new Vector3( Main.s_Instance.m_fMainTriggerBaseScale, Main.s_Instance.m_fMainTriggerBaseScale, 1.0f );
		SetSize( GetSize() * Main.s_Instance.m_fMainTriggerBaseScale );
		m_fUnfoldTimeLeft = Main.s_Instance.m_fMainTriggerUnfoldTime;
	} 

	public void UnfoldLoop()
	{
		if (!m_bIsUnfold) {
			return;
		}

		m_fUnfoldTimeLeft -= Time.deltaTime;
		if (m_fUnfoldTimeLeft <= 0.0f) {
			EndUnfold ();
		}
	}

	public void EndUnfold()
	{
		m_bIsUnfold = false;
		//outter.transform.localScale = new Vector3( 1.0f, 1.0f, 1.0f );
		SetSize( GetSize() / Main.s_Instance.m_fMainTriggerBaseScale );
		_trigger.transform.localScale = new Vector3( 1.0f, 1.0f, 1.0f );
	}

	public bool IsPking()
	{
		return ( m_ballOpponent != null );
	}

	string m_szName = "";
	public void SetName( string szName )
	{
		m_szName = szName;
	}

	public string GetName()
	{
		return m_szName;
	}

	// 内环正在膨胀
	public bool IsInnerBallGrowing()
	{
		return m_bIsInnerBallGrowing;
	}

	public void PK_Bean( Ball ballOpponent )
	{
		if (ballOpponent._balltype != eBallType.ball_type_bean) {
			return;
		}

		if (this._balltype != eBallType.ball_type_ball) {
			return;
		}

		EatBean ( (Bean)ballOpponent );
	}

	public void Pk( Ball ballOpponent )
	{
		return; // 废弃。走ProcessPk()流程

		if (m_bIsPreExploding) { // 处在“预爆炸”期
			return;
		}

		if (!ballOpponent.isThorn && this.photonView.ownerId != ballOpponent.photonView.ownerId) {
			;//return; // poppin test
		}

		if (ballOpponent._balltype == eBallType.ball_type_bean || this._balltype == eBallType.ball_type_bean) {
			PK_Bean ( ballOpponent );
			return;
		}

		if (ballOpponent.isThorn) {
			if ( ballOpponent.IsNewBallRunning() && ( ballOpponent.photonView.ownerId == this.photonView.ownerId )) {
				return;
			}
		}


		if ( IsDead() || ballOpponent.IsDead() )
		{
			//Debug.Log( "已死，不再参加PK" );
			return;
		}
		
		//Debug.Log ( this.GetName() + " 打算PK " + ballOpponent.GetName() );

		if (IsPking()) 
		{
			//Debug.Log ( this.GetName() + "正处在PK中，不能参与新的PK" );
			return;
		}


		if (ballOpponent.IsPking ()) 
		{
			//Debug.Log ( ballOpponent.GetName() + "正处在PK中，不能参与新的PK" );
			return;
		}

		// 中环膨胀状态中，不参与内部pk，但是依然要正常参与外部pk
		// 只要其中有一方是“刺”，那就甭管什么中环是不是在膨胀了
		// “中环”机制已经废弃，该微博“外壳机制”
		if ( ( IsShellShrink() || ballOpponent.IsShellShrink()/*IsInnerBallGrowing() || ballOpponent.IsInnerBallGrowing()*/ ) && ( !this.isThorn && !ballOpponent.isThorn ))
		{
			if (this.ownerId == ballOpponent.ownerId) { // 是同一个Player(队伍)的球
				return;
			}
		}	
		

		// 两球互为PK对手
		SetPkOppoent ( ballOpponent );
		ballOpponent.SetPkOppoent ( this );
	

		// 以Size论输赢
		float fSelfSize = GetSize ();
		float fOpponentSize = ballOpponent.GetSize ();
		if ( fSelfSize == fOpponentSize )
		{
			//Debug.Log( "平手，各回各家，各找各妈" );
			// 解除PK关系，别占着茅坑不拉屎
			if ( !this.isThorn && !ballOpponent.isThorn && this.photonView.ownerId == ballOpponent.photonView.ownerId ) // 同队的即便尺寸相等也要互吃
			{
				this.Eat( ballOpponent );  
			}
			else
			{
				SetPkOppoent( null );
				ballOpponent.SetPkOppoent ( null );
				return;
			}
		}
		else if ( fSelfSize > fOpponentSize ) // 甲吃乙
		{
			this.Eat (ballOpponent);
		}
		else // 乙吃甲
		{
				ballOpponent.Eat (this);

		}
		

	}

	float m_fDrawSize = 0.0f;
	float m_fDrawArea = 0.0f;
	// 先把对手的size吸过来
	public void DrawSize( float fSize )
	{
		m_fDrawArea += fSize * fSize;
	}

	// 消化掉吸进来的size
	public void DigestSize()
	{
		if (!photonView.isMine) {
			return;
		}

		//// check if can digest size now
		if (m_fDrawArea == 0.0f) {
			return;
		}

		if (m_bIsUnfold) { // 临时翼展期间不消化size
			return;
		}

		float fSelfSize = GetSize ();
		float fNewSize = Mathf.Sqrt( fSelfSize * fSelfSize + m_fDrawArea );	


		//Vector3 vecTemp = outter.transform.position;// 我日你个龟！ 改变scale也会导致position的变化！Unity这些逆天的Bug, 找谁说理去。
		//Debug.Log( "前：" + this.gameObject.transform.position + "," + outter.transform.position + " , " + outter.transform.localPosition );
		SetSize( fNewSize ); 

	//	vecTemp = outter.transform.position - vecTemp;
	//	Vector3 vecTemp1 = outter.transform.localPosition;
	//	vecTemp1 -= vecTemp;
	//	outter.transform.localPosition = vecTemp1;
	//	Debug.Log( "后：" + this.gameObject.transform.position + "," + outter.transform.position  + " , " + outter.transform.localPosition);


		m_fDrawArea = 0.0f; // 清零
	}
	
	// 吃掉对手
	public void Eat( Ball ballOpponent )
	{
		if (ballOpponent.isThorn) {
			EatThorn ( (Thorn)ballOpponent );
			return;
		}

		if (ballOpponent._balltype == eBallType.ball_type_bean) {
			EatBean ( (Bean)ballOpponent);
			return;
		}

		DrawSize (ballOpponent.GetSize ());

		float fSelfSize = GetSize ();
		float fOpponentSize = ballOpponent.GetSize ();
		float fNewSize = Mathf.Sqrt( fSelfSize * fSelfSize + fOpponentSize * fOpponentSize );		


		//SetSize( fNewSize ); // 我日你个龟！ 改变scale也会导致position的变化！Unity这些逆天的Bug, 找谁说理去。

	
	//	Vector3 vecTemp2 = outter.transform.position - vecTemp;
	//	Vector3 vecTemp1 = outter.transform.localPosition;
	//	vecTemp1 -= vecTemp2;
	//	outter.transform.localPosition = vecTemp1;

		// 如果被吃者身上带了刺，则原封不动的带到合球之后的球中
		float fOpponetThornSize = ballOpponent.GetCurThornTotalSize();
		float fEaterThornSize = GetCurThornTotalSize ();
		float fTotalThornSize = Mathf.Sqrt ( fOpponetThornSize * fOpponetThornSize + fEaterThornSize * fEaterThornSize );
		SetCurThornTotalSize ( fTotalThornSize );


		SetPkOppoent( null );

		DestroyBall( ballOpponent ); // 被吃的球球就销毁掉	
	}

	// 这个size是指半径
	public float CalculateMotherLeftSize( float fChildSize )
	{
		float fMotherLastSize = GetSize(); 
		float fLeftArea = fMotherLastSize * fMotherLastSize - fChildSize * fChildSize;
		if (fLeftArea < 0.0f) {
			return 0.0f;
		}
		return Mathf.Sqrt ( fLeftArea );
	}
		
	public bool CheckIfHaveTheSmallestSizeToSpitBall( ref float fMotherLeftSize )
	{
		fMotherLeftSize = CalculateMotherLeftSize (Main.BALL_MIN_SIZE);
		return fMotherLeftSize >= Main.BALL_MIN_SIZE;
	}

	public static Vector2[] s_aryBallExplodeDirection = { 
															new Vector2( 0.0f, 1.0f ),
															new Vector2( 0.5f, 0.5f ),
															new Vector2( 1.0f, 0.0f ),
															new Vector2( 0.5f, -0.5f ),
															new Vector2( 0, -1.0f ),
															new Vector2( -0.5f, -0.5f ),
															new Vector2( -1.0f, 0.0f ),
															new Vector2( -0.5f, 0.5f ),
	
														 };

	bool m_bIsPreExploding = false;
	float m_fPreExplodingTime = 3.0f;
	public void PreExplode()
	{
		if (!m_bIsPreExploding) {
			return;
		}

		m_fPreExplodingTime -= Time.deltaTime;

		if (m_fPreExplodingTime <= 0) {
			m_bIsPreExploding = false;
			Explode ();
			return;
		}
	}

	public void BeginPreExplode()
	{
		photonView.RPC ( "RPC_BeginPreExplode", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_BeginPreExplode()
	{
		m_bIsPreExploding = true;
		m_fPreExplodingTime = 3.0f;

		GameObject goEffect = GameObject.Instantiate (Main.s_Instance.m_preEffectPreExplode);
		goEffect.transform.parent =  outter.transform;
		Vector3 vecPos = new Vector3 ();
		vecPos = outter.transform.position;
		vecPos.z = -6.0f;
		goEffect.transform.position = vecPos;
		goEffect.transform.localScale = new Vector3 ( 1.0f, 1.0f, 1.0f );
		m_goPreExplodeEffect = goEffect;
	}

	 GameObject m_goPreExplodeEffect = null;

	// 球球爆炸
	public void Explode()
	{

		if (!photonView.isMine) {
			return;
		}

		if (m_bIsDead) {
			return;
		}

		SetCurThornTotalSize (0);

		// 爆炸特效
		GameObject goEffect = GameObject.Instantiate (Main.s_Instance.m_preEffectExplode);
		Vector3 vecPos = new Vector3 ();
		vecPos = outter.transform.position;
		vecPos.z = -6.0f;
		goEffect.transform.position = vecPos;
		float fSize = GetSize ();
		fSize = 3.0f * fSize / 5.0f;
		goEffect.transform.localScale = new Vector3 ( fSize, fSize, 1.0f );

		float fMotherSize = GetSize ();
		bool bAll = false;
		float fChildSize = Mathf.Sqrt ( 0.0625f ) * fMotherSize;
		float fMotherLeftSize = Mathf.Sqrt ( fMotherSize * fMotherSize - 8 * fChildSize * fChildSize );
		float fNum = 8;
		if (fChildSize < Main.BALL_MIN_SIZE) {
			fChildSize = Mathf.Sqrt ( 0.125f ) * fMotherSize;
			bAll = true;
		}
		if (fChildSize < Main.BALL_MIN_SIZE) {
			fNum = ( fMotherSize * fMotherSize ) / ( Main.BALL_MIN_SIZE * Main.BALL_MIN_SIZE );
			fChildSize = Main.BALL_MIN_SIZE;
			bAll = true;
		}
		GameObject goBall = null;
		Ball ball = null;
		int idx = 0;
		for (int i = 0; i < fNum; i++) {
			goBall = Main.s_Instance.PhotonInstantiate (Main.s_Instance.m_preBall);
			ball = goBall.GetComponent<Ball> ();
			ball.SetSize(fChildSize);
			ball.SetLine(Main.s_Instance.CreateLine());
			ball.gameObject.transform.parent = m_MainPlayer.gameObject.transform;
			Vector2 vecDire = s_aryBallExplodeDirection [idx++];
			float kx = vecDire.x;
			float ky = vecDire.y;
			ball.SetMoveDir ( kx, ky );
			this.CalculateNewBallBornPosAndRunDire( ball, kx, ky );
			ball.BeginNewBallRun ( Main.s_Instance.m_fNewExplosionBallRunSpeed );
		} // end for

		if (m_goPreExplodeEffect != null) {
			Destroy ( m_goPreExplodeEffect );
			m_goPreExplodeEffect = null;
		}

		if (bAll) {
			DestroyBall( this );
		} else {
			SetSize ( fMotherLeftSize );
		}
		/*
		float r_total = GetSize();
		float r_min = Main.BALL_MIN_SIZE;
		float fTotalArea = r_total * r_total;
		float fMinAreA = r_min * r_min;
		float n = (int)(fTotalArea / fMinAreA);

		float fKX = 0.0f, fKY = 0.0f;	
		GameObject goBall = null;
		Ball ball = null;
		int idx = 0;
		if (n > 8) {
			n = 8;
		}
		for (int i = 0; i < n; i++) {
			goBall = Main.s_Instance.PhotonInstantiate (Main.s_Instance.m_preBall);
			ball = goBall.GetComponent<Ball> ();

			ball.SetSize(Main.BALL_MIN_SIZE);
			ball.SetInnerBallSize( 0.5f );
			ball.SetLine(Main.s_Instance.CreateLine());
			//ball.BeginInnerBallGrow ();
			ball.BeginShellShrink();
			ball.gameObject.transform.parent = m_MainPlayer.gameObject.transform;
			if (idx >= 8) {
				idx = 0;
			}
			Vector2 vecDire = s_aryBallExplodeDirection [idx++];
			float kx = vecDire.x;
			float ky = vecDire.y;
			ball.SetMoveDir ( kx, ky );
			this.CalculateNewBallBornPosAndRunDire( ball, kx, ky );
			ball.BeginNewBallRun ( Main.s_Instance.m_fNewExplosionBallRunSpeed );
		} // end for

		float fLeftSize = fTotalArea - fMinAreA * n;
		Vector3 vecTemp = this.outter.transform.position;
		this.GetMoveDir ( ref fKX, ref fKY );
		DestroyBall( this );

		if (fLeftSize > 0.0f) {
			//SetSize( Mathf.Sqrt ( fLeftSize) );
			goBall = Main.s_Instance.PhotonInstantiate (Main.s_Instance.m_preBall);
			ball = goBall.GetComponent<Ball> ();
		    fLeftSize = Mathf.Sqrt (fLeftSize);
			if (fLeftSize < 1.0f) {
				fLeftSize = 1.0f;
			}
			ball.SetSize(fLeftSize);
			ball.SetInnerBallSize( 0.5f );
			ball.SetLine(Main.s_Instance.CreateLine());
			//ball.BeginInnerBallGrow ();
			ball.BeginShellShrink();
			ball.SetMoveDir ( fKX, fKY );
			ball.gameObject.transform.parent = m_MainPlayer.gameObject.transform;
			ball.gameObject.transform.position = vecTemp;
		} 

		*/
	}

	// 判断是否爆炸
	bool CheckIfExplodeNow()
	{
		// 刺的体积撑满当前的中环就爆炸（注意是当前实时的中环大小，并非中环的总大小。）
		float fMiddleRing = GetInnerBallSize() * GetSize();
		if ( m_fThornTotalSize > 0 && m_fThornTotalSize >= fMiddleRing )
		{
			return true;
		}

		return false;
	}

	// 吃豆子
	void EatBean( Bean bean )
	{
		/*
		float fLastSize = GetSize ();
		float fBeanSize = bean.GetBeanSize ();
		float fCurSize = Mathf.Sqrt( fLastSize * fLastSize + fBeanSize * fBeanSize );	
		SetSize ( fCurSize );
		*/
		DrawSize ( bean.GetBeanSize ());

		DestroyBall ( bean );
		SetPkOppoent( null );

		//Main.s_Instance.GenerateBean ();
	}

	//  吃掉刺
	void EatThorn( Thorn thorn )
	{
		float fMotherLeftSize = 0.0f;
		if (!CheckIfHaveTheSmallestSizeToSpitBall ( ref fMotherLeftSize )) { // 已经  不能分球了自然就不能吃刺
			return;
		}

		//if (m_fThornTotalSize >= (GetSize () * GetInnerBallSize ())) {
		//	return;
	//	}

		DestroyBall( thorn ); // 被吃的刺就销毁掉	
		SetPkOppoent( null );

		float fCurThornTotalSize = GetCurThornTotalSize (); // 内环（刺环）的半径的绝对数值
		float fThornSize = 3.0f;  // 刺的伤害具体数值是多少还有待于调 poppin test
		float fNewThornTotalSize = Mathf.Sqrt( m_fThornTotalSize * fCurThornTotalSize + fThornSize * fThornSize );
		IncreaseCurThornTotalSize ( fNewThornTotalSize ); 

		// 吃刺也要涨体积。具体涨多少待定
		DrawSize( 1.0f );
	}

	// 球球逻辑死亡
	bool m_bIsDead = false;
	public bool _dead
	{
		get { return m_bIsDead; }
		set { m_bIsDead = value; }
	}

	public void Die()
	{
		/*
		m_bIsDead = true;
		CircleCollider2D collider = outter.GetComponent<CircleCollider2D>();		
		if (collider != null) {
			collider.enabled = false;
		}

		PhotonView phoOutter =  outter.GetComponent<PhotonView> ();
		if (phoOutter != null) {
			phoOutter.enabled = false;
		}
		PhotonView phoBall =  this.gameObject.GetComponent<PhotonView> ();
		if (phoBall != null) {
			phoBall.enabled = false;
		}

		if (_isLocal) {
			Destroy (this.gameObject);
		} else {
			DestroyMe ();
		}

		DestroySpitBallTargetCircle ();
		*/
	}

	public void DestroyMe()
	{
		if (_isLocal) {
			this.gameObject.SetActive ( false );
			if (outter) {
				this._collider.enabled = false;
				this._colliderTrigger.enabled = false;
			}

			Destroy (this.gameObject);
		}
		else
		{
			photonView.RPC ("RPC_DestroyMe", PhotonTargets.All);
		}
	}

	[PunRPC]
	public void RPC_DestroyMe()
	{
		m_bIsDead = true;

		// 把该球球对应的线条也销毁掉(如果有线条的话)
		GameObject goLine = this.GetLine();
		if (goLine != null) 
		{
			//goLine.gameObject.transform.parent = m_goLinesPool.transform;
			goLine.SetActive (false);
			Destroy ( goLine );
		}

		if (outter) {
			this._collider.enabled = false;
			this._colliderTrigger.enabled = false;

			PhotonView phoOutter = outter.GetComponent<PhotonView> ();
			if (phoOutter != null) {
				phoOutter.enabled = false;
			}
		}

			if (photonView.isMine) {
				PhotonNetwork.Destroy (this.gameObject);
			}
	
		DestroySpitBallTargetCircle ();
		this.gameObject.SetActive ( false );
		//ball.Die();
		//ball.gameObject.SetActive ( false );
		//PhotonView photonView = ball.gameObject.GetComponent<PhotonView>();
		//ball.gameObject.transform.parent = m_goBallsPool.transform;
		//ball.gameObject.SetActive( false );
	}
	
	public bool IsDead()
	{
		return m_bIsDead;
	}

	bool m_bIsLocal = false;
	public bool _isLocal
	{
		set { m_bIsLocal = value; }
		get { return  m_bIsLocal; }
	}


	// 销毁一个球球
 	public void DestroyBall( Ball ball )
	{	
		ball.DestroyMe ();
	}

	public static GameObject m_goBallsPool; // 球球内存池
	public static GameObject m_goLinesPool; // 线条内存池
	public static void InitPools()
	{
		m_goBallsPool = GameObject.Find( "DestroyedBalls" );
		m_goLinesPool = GameObject.Find( "DestroyedLiness" );
	}

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		// poppin to do：这里明显需要优化：很多属性是不需要频繁同步的（对方客户端完全可以自己运算）。
		if(stream.isWriting) // MainPlayer
		{
			/*
			stream.SendNext(GetSize());
			stream.SendNext (GetInnerBallSize()  );
			stream.SendNext (m_bIsInnerBallGrowing);
			stream.SendNext (speed);
			*/
		}

		else // Other
		{
			/*
			float fSize = (float)stream.ReceiveNext ();
			float fInnerSize = (float)stream.ReceiveNext ();
			bool bIsInnerBallGrowing = (bool)stream.ReceiveNext ();
			speed = (float)stream.ReceiveNext ();

			SetSize (fSize);
			SetInnerBallSize ( fInnerSize );

			SetIfInnerBallGrowing ( bIsInnerBallGrowing );


			this.gameObject.SetActive (true);
			*/
		}	
	}

	int m_nId = 0;
	public void SetId( int nId )
	{
		m_nId = nId;
	}

	public int GetId()
	{
		return m_nId;
	}

	/*
	[PunRPC]
	public void ZouNi( Vector3 vecStartPos, float fKX, float fKY )
	{
		if (photonView.isMine) {
			return;
		}

		m_fNewBallRunKX = fKX;
		m_fNewBallRunKY = fKY;
		m_bNewBallRun = true;
		m_fNewBallRunSpeed = m_fNewBallInitSpeed;
		this.gameObject.transform.localPosition = vecStartPos;
		if (m_PhoTransView == null) {
			m_PhoTransView = GetComponent<PhotonTransformView> ();
		}
		m_PhoTransView._kx = m_fNewBallRunKX;
		m_PhoTransView._ky = m_fNewBallRunKY;
		m_PhoTransView.isNewBallRun = true;
	}

	[PunRPC]
	public void ZouNiEnd( Vector3 vecEndPosBall, Vector3 vecEndPosOutter )
	{
		if (photonView.isMine) {
			return;
		}

		this.transform.localPosition = vecEndPosBall;
		this.outter.transform.localPosition = vecEndPosOutter;

		m_bNewBallRun = false;
		m_PhoTransView.isNewBallRun = false;
	}
	*/

	public void SwitchSyncPositionMode( PhotonTransformViewPositionModel.InterpolateOptions type )
	{
		PhotonTransformView phTransView = this.gameObject.GetComponent<PhotonTransformView> ();
		phTransView.m_PositionControl.SetInterpolateType ( type );
	}

	// 计算新吐出的球（包括刺）初始位置和冲刺方向
	public void CalculateNewBallBornPosAndRunDire( Ball new_ball, float fKX, float fKY )
	{
		Vector3 vecTemp = this.outter.gameObject.transform.position;
		SpriteRenderer srMother = this.outter.GetComponent<SpriteRenderer>();
		SpriteRenderer srChild = new_ball.outter.GetComponent<SpriteRenderer>();

		float fMotherCurRadiusAbsVal = srMother.bounds.size.x  / 2.0f;
		float fChildCurRadiusAbsVal = srChild.bounds.size.x  / 2.0f;
		float fDis = fMotherCurRadiusAbsVal +  fChildCurRadiusAbsVal;
		vecTemp.x += fDis * fKX;
		vecTemp.y += fDis * fKY; 
		new_ball.SetMoveDir ( fKX, fKY );
		vecTemp.z = new_ball.transform.position.z;
		new_ball.gameObject.transform.position = vecTemp;
	}

	public void CalculateNewBallBornPosAndRunDire( Ball new_ball )
	{
		float fKX = 0.0f, fKY = 0.0f;
		this.GetMoveDir (ref fKX, ref fKY);
		CalculateNewBallBornPosAndRunDire ( new_ball, fKX, fKY );
	}

	public int m_nCasterId = 0;

	// 吐节奏球
	public RhythmBall SpitRhythmBall()
	{
		if (!photonView.isMine) {
			return null;
		}

		// 先判断能否吐刺，不能吐刺的话就没必要显示节奏球
		float fMotherCurRadius = 0.0f;
		if (!CheckIfCanSpitThorn ( ref fMotherCurRadius )) {
			return null;
		}

		GameObject goRhythmBall = GameObject.Instantiate (Main.s_Instance.m_preRhythm);
		RhythmBall rball = null;
		rball = goRhythmBall.GetComponent<RhythmBall> ();
		this.CalculateNewBallBornPosAndRunDire( rball );
		return rball;
	}

	public bool CheckIfCanSpitBall ()
	{
		if (m_bIsPreExploding) {
			return false;
		}

		return true;
	}
		
	// 吐刺
	public Thorn SpitThorn()
	{
		if (!photonView.isMine) {
			return null;
		}


		Thorn thorn = null; 
		float fMotherCurRadius = 0.0f;
		if (!CheckIfCanSpitThorn ( ref fMotherCurRadius )) {
			return null;
		}
			
		this.SetSize( fMotherCurRadius ); // 母球体积改变 poppin test

		GameObject go =  Main.s_Instance.PhotonInstantiate( Main.s_Instance.m_preThorn ) ; //  实例化一个“刺”
		go.transform.parent = Main.s_Instance.m_goThornsSpit.transform;
		go.transform.localScale = new Vector3 ( 1.8f, 1.8f, 1.0f );
		thorn = go.GetComponent<Thorn> ();
		thorn.Init (); // 注意，手动Init一下，不要放在Start()函数中init，因为Start()是对象创建之后的下一帧才执行，而不是立即执行。
		thorn.m_nCasterId = this.photonView.ownerId; // !!!!!!!!!!!!!!!!!!! 
		thorn._isSpited = true;

		// 跟“吐球”类似，刺吐出来之后也会往前方冲一段路
		this.CalculateNewBallBornPosAndRunDire( thorn );

		thorn.BeginNewBallRun ( Main.s_Instance.m_fNewThornRunSpeed );

		//Main.s_Instance.AddThornToThornList ( thorn );

		return thorn;
	}

	// 判断当前条件能否吐刺
	bool CheckIfCanSpitThorn( ref float fMotherCurRadius )
	{
		// 当前设定的吐刺条件就是：达到最低的吐球条件（因为一个刺就相当于是一个最小的球）
		// 我们设定的“最小”的球的scale值为1.0, 因此一个刺的scale值就是1.0
		// !!!! 再次强调：transform中的scale值，体现的是圆的半径，而不是圆的面积
		float fChildRadius = 1.0f;
		float fMotherLastRadius = GetSize ();
		float fPercent = (fChildRadius * fChildRadius) / (fMotherLastRadius * fMotherLastRadius);
		fMotherCurRadius = Mathf.Sqrt(1.0f - fPercent) * fMotherLastRadius;

		fMotherCurRadius = fMotherCurRadius * GetInnerBallSize ();
		if ( fMotherCurRadius < 1.0f || fMotherCurRadius < GetCurThornTotalSize() ) // 如果吐出一个刺之后，中环的scale值小于1.0，则说明这个母体的原体积不够大，根本不具备吐刺条件
		{
			return false;
		}


		if (m_bIsPreExploding) {
			return false;
		}

		return true;

	}


	float m_fCheckIfOutOfScreenInterval = 0.5f;
	float m_fCheckIfOutOfScreenTime = 0.0f;
	public bool CheckIfOutOfScreen()
	{
		Vector3 vecScreenPos = Main.s_Instance.m_MainCam.WorldToScreenPoint ( this.gameObject.transform.position );

		if (vecScreenPos.x < 0 || vecScreenPos.x > Screen.width || vecScreenPos.y < 0 || vecScreenPos.y > Screen.height) {
			return true;
		}

		return false;
	}
	public bool _isSpited = false;
	public void RefeshPosDueToOutOfScreen()
	{
		if (_balltype == eBallType.ball_type_thorn && _isSpited) {
			return;
		}


		if (m_fCheckIfOutOfScreenTime < m_fCheckIfOutOfScreenInterval) {
			m_fCheckIfOutOfScreenTime += Time.deltaTime;
			return;
		}

		m_fCheckIfOutOfScreenTime = 0.0f;

		if (!CheckIfOutOfScreen ()) {
			return;
		}

		this.gameObject.transform.position = Main.s_Instance.RandomPosWithinScreen ( 10.0f );
	}

	Vector3 m_vecSpitBallTemp = new Vector3 ();
	SpitBallTarget m_SpitBallTarget = null;
	public void ProcessSpitBallTarget( float fSpitBallPercent )
	{
		if (m_SpitBallTarget == null) {
			m_SpitBallTarget = (GameObject.Instantiate (Main.s_Instance.m_preSpitBallTarget)).GetComponent<SpitBallTarget>();
			m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
			m_SpitBallTarget.SetCaster ( this );
		}

		if (m_SpitBallTarget == null) {
			return;
		}

		if (fSpitBallPercent > 0.5f) {
			fSpitBallPercent = 0.5f;
		}

		float fMotherLeftScaleSize = 0.0f;
		if (!CheckIfHaveTheSmallestSizeToSpitBall (ref fMotherLeftScaleSize)) {
			return;
		}

		float fMotherLastBoundsSize = _srInnerFill.bounds.size.x;
		float fMotherLastScaleSize = this.GetSize ();
		float fBallBoundsSizePerScaleSize = fMotherLastBoundsSize / fMotherLastScaleSize;

		float fChildScaleSize = fMotherLastScaleSize * Mathf.Sqrt ( fSpitBallPercent ); 
		if (fChildScaleSize <= Main.BALL_MIN_SIZE) {
			fChildScaleSize = Main.BALL_MIN_SIZE;
		}

		float fChildBoundsSize = fChildScaleSize * fBallBoundsSizePerScaleSize;
		float fCurCircleScaleSize = m_SpitBallTarget.gameObject.transform.localScale.x;
		SpriteRenderer sr = m_SpitBallTarget.gameObject.GetComponent<SpriteRenderer> ();
		float fCurCircleBoundsSize = sr.bounds.size.x;
		float fCircleScaleSizePerBoundsSize = fCurCircleScaleSize / fCurCircleBoundsSize;
		float fCircleScaleSize = fCircleScaleSizePerBoundsSize * fChildBoundsSize;
		m_vecSpitBallTemp.x = m_vecSpitBallTemp.y = fCircleScaleSize;
		m_vecSpitBallTemp.z = 0.0f;
		m_SpitBallTarget.gameObject.transform.localScale = m_vecSpitBallTemp;

		float fMotherLeftArea = fMotherLastBoundsSize * fMotherLastBoundsSize - fChildBoundsSize * fChildBoundsSize;
		float fMotherLeftBoundsSize = Mathf.Sqrt ( fMotherLeftArea );
		float fChildStartPosX = outter.transform.position.x + (fMotherLeftBoundsSize + fChildBoundsSize) / 2.0f * _kx;
		float fChildStartPosY = outter.transform.position.y + (fMotherLeftBoundsSize + fChildBoundsSize) / 2.0f * _ky;

		// 匀变速直线运动的公式： v表示末速度
		// s = ( v ^ 2 - v0 ^ 2 ) / ( 2a )
		float v0 = Main.s_Instance.m_fNewBallRunSpeed;
		float a = Main.s_Instance.m_fNewBallRunSpeedAccelerate;
		float fDis = -v0 * v0 / (2 * a);
		float fTargetPosX = fChildStartPosX + fDis * _kx;
		float fTargetPosY = fChildStartPosY + fDis * _ky;
		m_vecSpitBallTemp.x = fTargetPosX;
		m_vecSpitBallTemp.y = fTargetPosY;
		m_vecSpitBallTemp.z = outter.transform.position.z;
		m_SpitBallTarget.gameObject.transform.position = m_vecSpitBallTemp;
	}

	public void DestroySpitBallTargetCircle ()
	{
		if (m_SpitBallTarget == null) {
			return;
		}

		GameObject.Destroy ( m_SpitBallTarget.gameObject );
		m_SpitBallTarget = null;
	}

	Ball m_ballCaster = null;
	public void SetCaster( Ball ballCaster )
	{
		m_ballCaster = ballCaster;
	}
}

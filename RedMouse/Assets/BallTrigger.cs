﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 接触

	void OnTriggerEnter2D(Collider2D other)
	{
		Ball ballOpponet = other.transform.parent.gameObject.GetComponent<Ball> ();
		Ball ballSelf = this.transform.parent.gameObject.GetComponent<Ball> ();
		ballSelf.ProcessPk ( ballOpponet );
	}

	void OnTriggerStay2D(Collider2D other)
	{
		Ball ballOpponet = other.transform.parent.gameObject.GetComponent<Ball> ();
		Ball ballSelf = this.transform.parent.gameObject.GetComponent<Ball> ();
		ballSelf.ProcessPk ( ballOpponet );
	}

	void OnTriggerExit2D(Collider2D other)
	{
		Ball ballOpponet = other.transform.parent.gameObject.GetComponent<Ball> ();
		Ball ballSelf = this.transform.parent.gameObject.GetComponent<Ball> ();

	}
}

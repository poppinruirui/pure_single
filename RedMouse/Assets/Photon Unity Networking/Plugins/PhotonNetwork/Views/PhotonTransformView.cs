﻿	// ----------------------------------------------------------------------------
// <copyright file="PhotonTransformView.cs" company="Exit Games GmbH">
//   PhotonNetwork Framework for Unity - Copyright (C) 2016 Exit Games GmbH
// </copyright>
// <summary>
//   Component to synchronize Transforms via PUN PhotonView.
// </summary>
// <author>developer@exitgames.com</author>
// ----------------------------------------------------------------------------

using UnityEngine;

/// <summary>
/// This class helps you to synchronize position, rotation and scale
/// of a GameObject. It also gives you many different options to make
/// the synchronized values appear smooth, even when the data is only
/// send a couple of times per second.
/// Simply add the component to your GameObject and make sure that
/// the PhotonTransformView is added to the list of observed components
/// </summary>
[RequireComponent(typeof(PhotonView))]
[AddComponentMenu("Photon Networking/Photon Transform View")]
public class PhotonTransformView : MonoBehaviour, IPunObservable
{
    //Since this component is very complex, we seperated it into multiple objects.
    //The PositionModel, RotationModel and ScaleMode store the data you are able to
    //configure in the inspector while the control objects below are actually moving
    //the object and calculating all the inter- and extrapolation

    [SerializeField]
    PhotonTransformViewPositionModel m_PositionModel = new PhotonTransformViewPositionModel();

    [SerializeField]
    PhotonTransformViewRotationModel m_RotationModel = new PhotonTransformViewRotationModel();

    [SerializeField]
    PhotonTransformViewScaleModel m_ScaleModel = new PhotonTransformViewScaleModel();

    public PhotonTransformViewPositionControl m_PositionControl;
    PhotonTransformViewRotationControl m_RotationControl;
    PhotonTransformViewScaleControl m_ScaleControl;

    PhotonView m_PhotonView;

    bool m_ReceivedNetworkUpdate = false;

	/// <summary>
	/// Flag to skip initial data when Object is instantiated and rely on the first deserialized data instead.
	/// </summary>
	bool m_firstTake = false;

    void Awake()
    {
        this.m_PhotonView = GetComponent<PhotonView>();

        this.m_PositionControl = new PhotonTransformViewPositionControl(this.m_PositionModel);
        this.m_RotationControl = new PhotonTransformViewRotationControl(this.m_RotationModel);
        this.m_ScaleControl = new PhotonTransformViewScaleControl(this.m_ScaleModel);

		m_vecCurStartPos = transform.position;

		m_Ball = this.gameObject.GetComponent<Ball> ();
    }

	void OnEnable()
	{
		m_firstTake = true;
	}

    void Update()
    {
		if (this.m_PhotonView == null || this.m_PhotonView.isMine == true || PhotonNetwork.connected == false)
        {
            return;
        }

        this.UpdatePosition();
        this.UpdateRotation();
        this.UpdateScale();
    }

	/*
    void UpdatePosition()
    {
        if (this.m_PositionModel.SynchronizeEnabled == false || this.m_ReceivedNetworkUpdate == false)
        {
            return;
        }
			
		transform.localPosition = this.m_PositionControl.UpdatePosition(transform.localPosition);
		Debug.Log( transform.localPosition );
    }
	*/


	float m_fNewBallRunKX = 0.0f;
	float m_fNewBallRunKY = 0.0f;
	bool m_bNewBallRun = false; 
	public bool isNewBallRun
	{
		get { return m_bNewBallRun; }
		set { m_bNewBallRun = value; }
	}

	float   m_fKX = 0.0f; // 方向系数
	public float _kx
	{
		get
		{
			if (!m_bNewBallRun) {
				return m_fKX;
			} else {
				return m_fNewBallRunKX;	
			}
		}
		set {
			if (m_bNewBallRun) {
				m_fNewBallRunKX = value;
			} else {
				m_fKX = value; 
			}
		}
	}
	float   m_fKY = 0.0f;
	public float _ky
	{
		get
		{
			if (!m_bNewBallRun) {
				return m_fKY;
			} else {
				return m_fNewBallRunKY;	
			}
		}
		set {
			if (m_bNewBallRun) {
				m_fNewBallRunKY = value;
			} else {
				m_fKY = value; 
			}
		}
	}
	Vector3 m_vecTemp = new Vector3();

	Vector3 m_vecCurStartPos = new Vector3();

	Ball m_Ball = null;
	/*
	// t:[0, 1]
	public static Vector3 PoppinLerp(Vector3 from, Vector3 to, float t)
	{
		
		return Vector3.Lerp ( from, to, t );
	}

	void UpdatePosition()
	{
		if (this.m_PositionModel.SynchronizeEnabled == false || this.m_ReceivedNetworkUpdate == false)
		{
			return;
		}

		if (m_Ball != null) {
			if (m_Ball.IsNewBallRunning ()) {
				return;
			}
		}


		switch( this.m_PositionControl.GetInterpolateType()  )
		{
			case PhotonTransformViewPositionModel.InterpolateOptions.Disabled: 	
			{
				m_vecTemp = this.m_PositionControl.GetNetworkPosition (); // new position synced by net work
				if (true) {
					m_vecLastPos = m_vecTemp;
					transform.localPosition = m_vecTemp;
				} else {
					//Debug.Log ( "我猜的果然没错" );
				}
			}
			break;

			case PhotonTransformViewPositionModel.InterpolateOptions.Lerp:
			{
				m_vecTemp = this.m_PositionControl.GetNetworkPosition (); // new position synced by net work
				if (m_vecTemp != m_vecCurStartPos) {
					StartNewLerpRound (m_vecTemp);
				} else {
					DoPoppinLerp ();
				}
			}
			break;

		} // end switch
	}

	void StartNewLerpRound( Vector3 vecNewTargetPos)
	{
		m_vecCurStartPos = m_vecLastTargetPos;
		transform.localPosition = m_vecLastTargetPos;
		m_vecCurTargetPos = vecNewTargetPos;
		journeyLength = Vector3.Distance(m_vecCurStartPos, m_vecCurTargetPos);
		m_fCurTime = 0.0f;
	}

	void DoPoppinLerp()
	{
		m_fCurTime += Time.deltaTime;
		if ( m_Ball == null || m_Ball.speed == 0) {   
			transform.localPosition = m_vecCurStartPos;
			return;
		}

		float distCovered = m_fCurTime * m_Ball.speed / 0.02f;
		float t = distCovered / journeyLength; 
			t = 1.0f;
		}

		transform.localPosition =  PoppinLerp ( m_vecCurStartPos, m_vecCurTargetPos, t );
	}
*/


	// 计算运行方向
	void CalculateMoveDirect( float fSrcX, float fSrcY, float fDestX, float fDestY, ref float fKX, ref float fKY)
	{
		float fDeltaX = fDestX - fSrcX;
		float fDeltaY = fDestY - fSrcY;
		float fDis = Mathf.Sqrt( fDeltaX * fDeltaX + fDeltaY * fDeltaY ); 
		if ( fDis == 0.0f )
		{
			fKX = 0;
			fKY = 0;
		}
		else
		{
			fKX = fDeltaX / fDis;
			fKY = fDeltaY / fDis;
		}
	}
	
	void UpdatePosition()
	{
		if (m_Ball != null) {
			if (m_Ball.IsNewBallRunning ()) {
				return;
			}
		}


		switch( this.m_PositionControl.GetInterpolateType()  )
		{
		case PhotonTransformViewPositionModel.InterpolateOptions.Disabled: 	
			{
				transform.localPosition = this.m_PositionControl.GetNetworkPosition (); // new position synced by net work

			}
			break;

		case PhotonTransformViewPositionModel.InterpolateOptions.EstimatedSpeed:
			{
				m_vecTemp = this.m_PositionControl.GetNetworkPosition ();	
				if (m_vecTemp != m_vecCurStartPos) {
					StartNewEstimateSpeedRound (m_vecTemp);

				} else {
					DoEstimateSpeed ();
				}
			}
			break;

		} // end switch


	}
	
	void Wait()   
	{
		if (m_bNeedWaitX) {
		m_fWaitDistanceX -= Time.deltaTime * m_Ball.speed  * _kx;

			if (m_fWaitDistanceX <= 0.0f) {
				m_bNeedWaitX = false;
			}
		}


		if (m_bNeedWaitY) {
		m_fWaitDistanceY -= Time.deltaTime * m_Ball.speed  * _ky;
			if (m_fWaitDistanceY <= 0.0f) {
				m_bNeedWaitY = false;
			}
		}
	}

	// 返回值：1表示方向相同，且为正方向； -1表示方向相同，且为负方向； 0表示方向不同

	int CheckIfSameDire( float a, float b )
	{
		if (a > 0 && b > 0) {
			return 1;
		}

		if (a < 0 && b < 0) {
			return -1;
		}

		return 0;
	}
	
	bool m_bNeedWaitX = false;
	bool m_bNeedWaitY = false;
	int m_nSameDireX = 0;
	int m_nSameDireY = 0;
	float m_fWaitDistanceX = 0.0f;
	float m_fWaitDistanceY = 0.0f; 
float m_fTempSpeedX = 0.0f;
float m_fTempSpeedY = 0.0f;
bool CheckIfNeedWait( float fCurDir, float fCurPos, float fNewRoundDir, float fNewRoundStartPos, ref int nSameDire )
	{
		nSameDire = CheckIfSameDire ( fCurDir, fNewRoundDir );
		if (nSameDire == 0 ) {  // 新旧方向不同，无需等待，直接赋值
			return false;
		}

	if (nSameDire == 1) { // 方向相同且为正方向
			if (fCurPos > fNewRoundStartPos) {
				return true;
			}
	} else if (nSameDire == -1) { // 方向相同且为负方向
			if (fCurPos < fNewRoundStartPos) {
				return true;
		}

		}

		return false;
	}

	void StartNewEstimateSpeedRound( Vector3 vecNewRoundStartPos )
	{
		float fLastRoundStartPosX = m_vecCurStartPos.x;
		float fLastRoundStartPosY = m_vecCurStartPos.y;
		m_vecCurStartPos = vecNewRoundStartPos;
		 
		//transform.localPosition = m_vecCurStartPos;
		float fLastKX = _kx, fLastKY = _ky;
		float kx = 0.0f, ky = 0.0f;
	CalculateMoveDirect (fLastRoundStartPosX, fLastRoundStartPosY, m_vecCurStartPos.x, m_vecCurStartPos.y, ref kx, ref ky);
		_kx = kx;
		_ky = ky;
		
	m_bNeedWaitX = CheckIfNeedWait ( fLastKX,  transform.localPosition.x, _kx, m_vecCurStartPos.x, ref m_nSameDireX );
	m_bNeedWaitY = CheckIfNeedWait ( fLastKY,  transform.localPosition.y, _ky, m_vecCurStartPos.y, ref m_nSameDireY );

		if (!m_bNeedWaitX) {
			m_vecTemp = transform.localPosition;
			m_vecTemp.x = m_vecCurStartPos.x;
			transform.localPosition = m_vecTemp;
		} else {
		m_fWaitDistanceX = Mathf.Abs( transform.localPosition.x - m_vecCurStartPos.x );
		m_fTempSpeedX = m_Ball.speed;
		}

		if (!m_bNeedWaitY) {
			m_vecTemp = transform.localPosition;
			m_vecTemp.y = m_vecCurStartPos.y;
			transform.localPosition = m_vecTemp;
		} else {
			m_fWaitDistanceY = Mathf.Abs( transform.localPosition.y - m_vecCurStartPos.y );
		m_fTempSpeedY = m_Ball.speed;
		}

		
	}

		void DoEstimateSpeed()
		{
			if (m_Ball == null) {
				return;
			}

			m_vecTemp = this.transform.localPosition;
		if (!m_bNeedWaitX) {
		m_vecTemp.x += m_Ball.speed * Time.deltaTime * _kx;
		} else {
		
		m_vecTemp.x += m_fTempSpeedX * Time.deltaTime * _kx;
			m_fTempSpeedX -= 0.01f;
		}
		if (!m_bNeedWaitY) {
			m_vecTemp.y += m_Ball.speed * Time.deltaTime * _ky;

		} else {
		m_vecTemp.y += m_fTempSpeedY * Time.deltaTime * _ky;
		m_fTempSpeedY -= 0.01f;
		}

			this.transform.localPosition = m_vecTemp;

			Wait ();	

		}


    void UpdateRotation()
    {
        if (this.m_RotationModel.SynchronizeEnabled == false || this.m_ReceivedNetworkUpdate == false)
        {
            return;
        }

        transform.localRotation = this.m_RotationControl.GetRotation(transform.localRotation);
    }

    void UpdateScale()
    {
        if (this.m_ScaleModel.SynchronizeEnabled == false || this.m_ReceivedNetworkUpdate == false)
        {
            return;
        }

        transform.localScale = this.m_ScaleControl.GetScale(transform.localScale);
    }

    /// <summary>
    /// These values are synchronized to the remote objects if the interpolation mode
    /// or the extrapolation mode SynchronizeValues is used. Your movement script should pass on
    /// the current speed (in units/second) and turning speed (in angles/second) so the remote
    /// object can use them to predict the objects movement.
    /// </summary>
    /// <param name="speed">The current movement vector of the object in units/second.</param>
    /// <param name="turnSpeed">The current turn speed of the object in angles/second.</param>
    public void SetSynchronizedValues(Vector3 speed, float turnSpeed)
    {
        this.m_PositionControl.SetSynchronizedValues(speed, turnSpeed);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        this.m_PositionControl.OnPhotonSerializeView(transform.localPosition, stream, info);
        this.m_RotationControl.OnPhotonSerializeView(transform.localRotation, stream, info);
        this.m_ScaleControl.OnPhotonSerializeView(transform.localScale, stream, info);

        if (this.m_PhotonView.isMine == false && this.m_PositionModel.DrawErrorGizmo == true)
        {
            this.DoDrawEstimatedPositionError();
        }

        if (stream.isReading == true)
        {
            this.m_ReceivedNetworkUpdate = true;

			// force latest data to avoid initial drifts when player is instantiated.
			if (m_firstTake)
			{
				m_firstTake = false;

				if (this.m_PositionModel.SynchronizeEnabled)
				{
					this.transform.localPosition = this.m_PositionControl.GetNetworkPosition();
				}

				if (this.m_RotationModel.SynchronizeEnabled)
				{
					this.transform.localRotation = this.m_RotationControl.GetNetworkRotation();
				}

				if (this.m_ScaleModel.SynchronizeEnabled)
				{
					this.transform.localScale = this.m_ScaleControl.GetNetworkScale();
				}

			}

        }
    }

    //void OnDrawGizmos()
    //{
    //    if( Application.isPlaying == false || m_PhotonView == null || m_PhotonView.isMine == true || PhotonNetwork.connected == false )
    //    {
    //        return;
    //    }

    //    DoDrawNetworkPositionGizmo();
    //    DoDrawExtrapolatedPositionGizmo();
    //}

    void DoDrawEstimatedPositionError()
    {
        Vector3 targetPosition = this.m_PositionControl.GetNetworkPosition();

		// we are synchronizing the localPosition, so we need to add the parent position for a proper positioning.
		if (transform.parent != null)
		{
			targetPosition = transform.parent.position + targetPosition ;
		}

		Debug.DrawLine(targetPosition, transform.position, Color.red, 2f);
        Debug.DrawLine(transform.position, transform.position + Vector3.up, Color.green, 2f);
		Debug.DrawLine(targetPosition , targetPosition + Vector3.up, Color.red, 2f);
    }

    //void DoDrawNetworkPositionGizmo()
    //{
    //    if( m_PositionModel.DrawNetworkGizmo == false || m_PositionControl == null )
    //    {
    //        return;
    //    }

    //    ExitGames.Client.GUI.GizmoTypeDrawer.Draw( m_PositionControl.GetNetworkPosition(),
    //                                               m_PositionModel.NetworkGizmoType,
    //                                               m_PositionModel.NetworkGizmoColor,
    //                                               m_PositionModel.NetworkGizmoSize );
    //}

    //void DoDrawExtrapolatedPositionGizmo()
    //{
    //    if( m_PositionModel.DrawExtrapolatedGizmo == false ||
    //        m_PositionModel.ExtrapolateOption == PhotonTransformViewPositionModel.ExtrapolateOptions.Disabled ||
    //        m_PositionControl == null )
    //    {
    //        return;
    //    }

    //    ExitGames.Client.GUI.GizmoTypeDrawer.Draw( m_PositionControl.GetNetworkPosition() + m_PositionControl.GetExtrapolatedPositionOffset(),
    //                                               m_PositionModel.ExtrapolatedGizmoType,
    //                                               m_PositionModel.ExtrapolatedGizmoColor,
    //                                               m_PositionModel.ExtrapolatedGizmoSize );
    //}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	// 接触
	void OnCollisionEnter2D(Collision2D coll)
	{
		return;

		Ball ballOpponet = coll.transform.parent.gameObject.GetComponent<Ball> ();
		Ball ballSelf = this.transform.parent.gameObject.GetComponent<Ball> ();
		PhotonView pho = ballSelf.gameObject.GetComponent<PhotonView> ();
		int dhit = pho.ownerId;
		ballSelf.Pk ( ballOpponet );
	}
		
	// 胶着状态(继续PK)
	void OnCollisionStay2D(Collision2D coll)
	{
		return;

		Ball ballOpponet = coll.transform.parent.gameObject.GetComponent<Ball> ();
		Ball ballSelf = this.transform.parent.gameObject.GetComponent<Ball> ();
		ballSelf.Pk ( ballOpponet );
	}
	
	// 分离
	void OnCollisionExit2D(Collision2D coll)
	{
		return;

		Ball ballOpponet = coll.transform.parent.gameObject.GetComponent<Ball> ();
		Ball ballSelf = this.transform.parent.gameObject.GetComponent<Ball> ();
		ballSelf.SetPkOppoent( null );
		ballOpponet.SetPkOppoent( null );
	}
	
}
